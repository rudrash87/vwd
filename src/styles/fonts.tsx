import localFont from 'next/font/local';

export const montblack = localFont({
  src: "../../public/fonts/MontBlack/font.woff2",
  variable: "--font-montblack",
  display: "swap",
});

export const montblackitalic = localFont({
  src: "../../public/fonts/MontBlackItalic/font.woff2",
  variable: "--font-montblackitalic",
  display: "swap",
});

export const montbold = localFont({
  src: "../../public/fonts/MontBold/font.woff2",
  variable: "--font-montbold",
  display: "swap",
});

export const montbolditalic = localFont({
  src: "../../public/fonts/MontBoldItalic/font.woff2",
  variable: "--font-montbolditalic",
  display: "swap",
});

export const montbook = localFont({
  src: "../../public/fonts/MontBook/font.woff2",
  variable: "--font-montbook",
  display: "swap",
});

export const montbookitalic = localFont({
  src: "../../public/fonts/MontBookItalic/font.woff2",
  variable: "--font-montbookitalic",
  display: "swap",
});

export const montextralight = localFont({
  src: "../../public/fonts/MontExtraLight/font.woff2",
  variable: "--font-montextralight",
  display: "swap",
});

export const montextralightitalic = localFont({
  src: "../../public/fonts/MontExtraLightItalic/font.woff2",
  variable: "--font-montextralightitalic",
  display: "swap",
});

export const monthairline = localFont({
  src: "../../public/fonts/MontHairline/font.woff2",
  variable: "--font-monthairline",
  display: "swap",
});

export const monthairlineitalic = localFont({
  src: "../../public/fonts/MontHairlineItalic/font.woff2",
  variable: "--font-monthairlineitalic",
  display: "swap",
});

export const montheavy = localFont({
  src: "../../public/fonts/MontHeavy/font.woff2",
  variable: "--font-montheavy",
  display: "swap",
});

export const montheavyitalic = localFont({
  src: "../../public/fonts/MontHeavyItalic/font.woff2",
  variable: "--font-montheavyitalic",
  display: "swap",
});

export const montlight = localFont({
  src: "../../public/fonts/MontLight/font.woff2",
  variable: "--font-montlight",
  display: "swap",
});

export const montlightitalic = localFont({
  src: "../../public/fonts/MontLightItalic/font.woff2",
  variable: "--font-montlightitalic",
  display: "swap",
});

export const montregular = localFont({
  src: "../../public/fonts/MontRegular/font.woff2",
  variable: "--font-montregular",
  display: "swap",
});

export const montregularitalic = localFont({
  src: "../../public/fonts/MontRegularItalic/font.woff2",
  variable: "--font-montregularitalic",
  display: "swap",
});

export const montsemibold = localFont({
  src: "../../public/fonts/MontSemiBold/font.woff2",
  variable: "--font-montsemibold",
  display: "swap",
});

export const montsemibolditalic = localFont({
  src: "../../public/fonts/MontSemiBoldItalic/font.woff2",
  variable: "--font-montsemibolditalic",
  display: "swap",
});

export const montthin = localFont({
  src: "../../public/fonts/MontThin/font.woff2",
  variable: "--font-montthin",
  display: "swap",
});

export const montthinitalic = localFont({
  src: "../../public/fonts/MontThinItalic/font.woff2",
  variable: "--font-montthinitalic",
  display: "swap",
});