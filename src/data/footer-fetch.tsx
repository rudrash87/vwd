"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import Footer from "@/components/footer";
export default function FooterJson() {
  const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
  const options = { headers: { Authorization: `Bearer ${token}` } };

  const [data, setData] = useState<any>([]);
  const [isLoading, setLoading] = useState(true);
  const [globaldata, setGlobalData] = useState<any>([]);
  const [otherdata, setOtherData] = useState<any>([]);
  const [secondarydata, setSecondaryData] = useState<any>([]);


  const fetchData = useCallback(async (start: number, limit: number) => {
    setLoading(true);
    try {
      const path = `/footer-menus`;
      const urlParamsObject = {
        populate: {
          MenuGroup: { populate: "*" },
        },
        pagination: {
          start: start,
          limit: limit,
        },
      };
      const responseData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setData(responseData.data);
      } else {
        setData((prevData: any[]) => [...prevData, ...responseData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }

    setLoading(true);
    try {
      const path = `/global`;
      const urlParamsObject = {
        populate: {
          SocialConfig: { populate: "*" },
        },
        pagination: {
          start: start,
          limit: limit,
        },
      };
      const responseGlobalData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setGlobalData(responseGlobalData.data);
      } else {
        setGlobalData((prevData: any[]) => [...prevData, ...responseGlobalData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }

    setLoading(true);
    try {
      const path = `/global`;
      const urlParamsObject = {
        populate: {
          otherWebsites: { 
            populate: {
              website: {
                populate: "*"
              }
            }
          },
        },
        pagination: {
          start: start,
          limit: limit,
        },
      };
      const responseGlobalData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setOtherData(responseGlobalData.data);
      } else {
        setOtherData((prevData: any[]) => [...prevData, ...responseGlobalData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }

    setLoading(true);
    try {
      const path = `/footer-secondary-menus`;
      const urlParamsObject = {
        populate: {
          link: { populate: "*" },
        },
        pagination: {
          start: start,
          limit: limit,
        },
      };
      const responseData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setSecondaryData(responseData.data);
      } else {
        setSecondaryData((prevData: any[]) => [...prevData, ...responseData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }

  }, []);


  useEffect(() => {
    fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
  }, [fetchData]);


  if (isLoading) return "Loading";

  return (
    <Footer data={data} globaldata={globaldata} otherdata={otherdata} secondarydata={secondarydata}></Footer>
  );
}