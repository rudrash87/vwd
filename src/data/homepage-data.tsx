"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import HomeBannerSection from "@/app/Homesubpages/Homebannersection";
import HomeCta from "@/app/Homesubpages/Homecta";
import ProcessSection from "@/app/Homesubpages/ProcessSection";
import TechSection from "@/app/Homesubpages/TechSection";
import RecentProject from "@/app/Homesubpages/RecentProject";
import ArticleRecent from "@/app/Homesubpages/ArticleRecent";

export default function Homebanner() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    headerSection: { populate: "*" },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <HomeBannerSection bannerdata={data}></HomeBannerSection>
    );
}

export function BottomCta() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [ctadata, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    ctaSection: { populate: "*" },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <HomeCta ctadata={ctadata}></HomeCta>
    );
}

export function ProcessSectionData() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [processdata, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    processSection: { 
                        populate: {
                            titleContentImageLink: {
                                populate: "*",
                            },
                        }
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <ProcessSection data={processdata}></ProcessSection>
    );
}

export function TechSectionData() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [techdata, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    techSection: { 
                        populate: {
                            technology: {
                                populate: "*",
                            },
                            link: {
                                populate: "*",
                            },
                        }
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <TechSection data={techdata}></TechSection>
    );
}

export function ProjectSectionData() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [recentprojectdata, setData] = useState<any>([]);
    const [projectdataarray, setArrayData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    projectSection: { 
                        populate: "*",
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    const projectfetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/projects`;
            const urlParamsObject = {
                sort: { releaseDate: "desc" },
                populate: "*",
                pagination: {
                    start: 0,
                    limit: 5,
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setArrayData(responseData.data);
            } else {
                setArrayData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);



    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);

    useEffect(() => {
        projectfetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [projectfetchData]);


    if (isLoading) return "Loading";

    return (
        <RecentProject data={recentprojectdata} recentdata={projectdataarray}></RecentProject>
    );
}

export function ArticleSectionData() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [recentarticledata, setData] = useState<any>([]);
    const [recentarticlearray, setArrayData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/homepage`;
            const urlParamsObject = {
                populate: {
                    articleSection: { 
                        populate: "*",
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);

    const fetchArrayData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/articles`;
            const urlParamsObject = {
                sort: { publicationDate: "desc" },
                populate: "*",
                pagination: {
                    start: 0,
                    limit: 10,
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setArrayData(responseData.data);
            } else {
                setArrayData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);

    useEffect(() => {
        fetchArrayData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchArrayData]);


    if (isLoading) return "Loading";

    return (
        <ArticleRecent data={recentarticledata} articledata={recentarticlearray}></ArticleRecent>
    );
}