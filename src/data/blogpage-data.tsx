"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import BlogbannerSection from "@/app/blogsubpages/blogbanner";
import CategoryList from "@/components/sections/category-list";
import Bloglisting from "@/components/sections/bloglisting";
import RecentArticle from "@/components/sections/recent-article";

export default function Blogbanner() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/blog-page`;
            const urlParamsObject = {
                populate: {
                    title: { populate: "*" },
                    description: { populate: "*"},
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <BlogbannerSection data={data}></BlogbannerSection>
    );
}

export function BlogcatData(Props:any) {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };
    const [catdata, setData] = useState<any>([]);
    const [isvisionLoading, setVisonLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setVisonLoading(true);
        try {
            const path = `/article-categories`;
            const urlParamsObject = {
                populate: "*",
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setVisonLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isvisionLoading) return "Loading";

    return (
        <CategoryList catdata={catdata} selectedGenre={Props.catsele} onSelect={Props.cselect} />
    );
}

export function BlogListingData(Props:any) {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };
    const [articledata, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);

    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/articles`;
            const urlParamsObject = {
                sort: { publicationDate: "desc" },
                populate: {
                    relatedCategories: {
                        populate: "*"
                    },
                    image: {
                        populate : "*"
                    }
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);

    if (isLoading) return "Loading";

    return (
        <Bloglisting selectedGenre={Props.catsele} catdata={articledata} />
    );
}

export function BlogRecentData(Props:any) {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };
    const [articledata, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);
    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/articles`;
            if (Props.slug){ 
                const urlParamsObject = {
                    populate: "*",
                    filters:{
                        slug : Props.slug
                    }
                }
                const responseData = await fetchAPI(path, urlParamsObject, options);
                if (start === 0) {
                    setData(responseData.data);
                } else {
                    setData((prevData: any[]) => [...prevData, ...responseData.data]);
                }
            } else {
                const urlParamsObject = {
                    sort: { publicationDate: "desc" },
                    populate: {
                        relatedCategories: {
                            populate: "*"
                        },
                        image: {
                            populate: "*"
                        },
                        content: {
                            populate: "*"
                        }
                    },
                    pagination: {
                        start: 0,
                        limit: 2,
                    },
                };
                const responseData = await fetchAPI(path, urlParamsObject, options);
                if (start === 0) {
                    setData(responseData.data);
                } else {
                    setData((prevData: any[]) => [...prevData, ...responseData.data]);
                }
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);

    if (isLoading) return "Loading";

    return (
        <RecentArticle className="" articledata={articledata} title={Props.slug? "D'autre articles" : "Forfait Développeur"}></RecentArticle>
    );
}