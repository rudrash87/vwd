import { faker } from "@faker-js/faker";

export function getAccordion(count: number): any[] {
  const animations = [];
  for (let i = 0; i < count; i++) {
    const animation = {
      id: i,
      title: faker.lorem.words(2),
      description: faker.lorem.paragraph(),
    };
    animations.push(animation);
  }
  return animations;
}


export function getImagesData(count: number): any[] {
  const imadata = [];
  for (let i = 0; i < count; i++) {
    const animation = {
      id: i,
      image: faker.image.url(),
    };
    imadata.push(animation);
  }
  return imadata;
}


export function getSwiperData(count: number): any[] {
  const animations = [];
  for (let i = 0; i < count; i++) {
    const animation = {
      id: i,
      title: faker.lorem.words(2),
      description: faker.lorem.paragraph(),
      image: faker.image.url(),
      link: faker.lorem.word(),
      date: "20/06/2023",
    };
    animations.push(animation);
  }
  return animations;
}

export function getImageText(count: number): any[] {
  const positions = ["right", "reverse"]; // Array of possible positions
  const animations = [];
  for (let i = 0; i < count; i++) {
    const animation = {
      mediaPosition: positions[i % positions.length],
      title: faker.lorem.words(2),
      toptitle: faker.lorem.words(2),
      multicolor: "",
      description: faker.lorem.paragraph(),
      image: faker.image.url(),
      link: faker.lorem.words(2),
    };
    animations.push(animation);
  }
  return animations;
}

export function getHomeImageText(count: number): any[] {
  const positions = ["default", "reverse"]; // Array of possible positions
  const animations = [];
  for (let i = 0; i < count; i++) {
    const animation = {
      position: positions[i % positions.length],
      title: faker.lorem.words(2),
      description: faker.lorem.paragraph(),
      image: faker.image.url(),
      link: faker.lorem.words(2),
    };
    animations.push(animation);
  }
  return animations;
}

export function getMoreimages(count: number): any[] {
  const images = [];
  for (let i = 0; i < count; i++) {
    const image = {
      url: faker.image.url(),
    };
    images.push(image);
  }
  return images;
}

export function getPortfolioImages(count: number): any[] {
  const images = [];
  for (let i = 0; i < count; i++) {
    const image = {
      src: faker.image.url(),
      url: faker.image.url(),
      title: faker.lorem.words(2),
      text: faker.lorem.paragraph(),
    };
    images.push(image);
  }
  return images;
}

export function getBlogs(count: number): any[] {
  const categories = Array.from({ length: 3 }, () =>
    faker.commerce.department()
  );

  const blogsdata = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(5),
      description: faker.lorem.text(),
      tagTitle: faker.lorem.word(4),
      image: faker.image.url(),
      category: {
        name: categories[Math.floor(Math.random() * categories.length)], // Sélectionner une catégorie au hasard
      },
    };
    blogsdata.push(exhibitor);
  }
  return blogsdata;
}

export function getMegamenu(count: number): any[] {
  const megamenudata = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(2),
      description: faker.lorem.words(6),
      image: faker.image.url(),
      url: faker.image.url(),
    };
    megamenudata.push(exhibitor);
  }
  return megamenudata;
}

export function getPriceCard(count: number): any[] {
  const pricecard = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(2),
      description: faker.lorem.text(),
      price: 90,
      subtext: faker.lorem.words(2),
      link1: faker.image.url(),
      label1: "Proposer un brief",
      link2: faker.image.url(),
      label2: "Planifier une session",
    };
    pricecard.push(exhibitor);
  }
  return pricecard;
}

export function getTeamdata(count: number): any[] {
  const teamcard = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      name: faker.lorem.words(2),
      designation: faker.lorem.words(1),
      image: faker.image.url(),
    };
    teamcard.push(exhibitor);
  }
  return teamcard;
}

export function getVisiondata(count: number): any[] {
  const visioncard = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(2),
      text: faker.lorem.text(),
      image: faker.image.url(),
      visionitems: [
        {
          label: faker.lorem.words(2),
          text: faker.lorem.words(2),
        },
        {
          label: faker.lorem.words(2),
          text: faker.lorem.words(2),
        },
        {
          label: faker.lorem.words(2),
          text: faker.lorem.words(2),
        },
      ]
    };
    visioncard.push(exhibitor);
  }
  return visioncard;
}

export function getBasicdata(count: number): any[] {
  const basicdetails = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(2),
      text: faker.lorem.text(),
    };
    basicdetails.push(exhibitor);
  }
  return basicdetails;
}

export function getReviewdata(count: number): any[] {
  const reviewarray = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      text: faker.lorem.text(),
      designation: faker.lorem.words(2),
      name: faker.lorem.words(2),
    };
    reviewarray.push(exhibitor);
  }
  return reviewarray;
}

export function getTechdescriptiondata(count: number): any[] {
  const techdescriptionarray = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      title: faker.lorem.words(1),
      name: faker.lorem.words(2),
    };
    techdescriptionarray.push(exhibitor);
  }
  return techdescriptionarray;
}

export function getArticelBannerdata(count: number): any[] {
  const articlebannerarray = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      id: i,
      cat: "Growth",
      title: "Titre d'un article lorem ipsum dolor sit amet",
      date: "20/06/2023",
      auth_image: faker.image.url(),
      written_by: "Nom Prénom",
      main_image: faker.image.url(),
    };
    articlebannerarray.push(exhibitor);
  }
  return articlebannerarray;
}

export function getPostBySlug(slug: string) {
  type Items = {
    [key: string]: string
  }
  return {
    title: "UI design", 
    content: "content"
  }
}

export function getAllTechnologyPosts() {
  const count = 1;
  const posts = [];
  for (let i = 0; i < count; i++) {
    const exhibitor = {
      slug: "ui-design",
      title: "UX design",
    };
    posts.push(exhibitor);
  }
  return posts;
}