"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import AgencebannerSection from "@/app/agencesubpages/agencybanner";
import AgenceCTASection from "@/app/agencesubpages/agencecta";
import AgenceTeamSection from "@/app/agencesubpages/teamsection";
import AgenceValeurSection from "@/app/agencesubpages/aboutvaleur";
import AgenceVisionSection from "@/app/agencesubpages/aboutvision";
export default function Aboutbanner() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/about-page`;
            const urlParamsObject = {
                populate: {
                    title: { populate: "*" },
                    description: { populate: "*"},
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <AgencebannerSection data={data}></AgencebannerSection>
    );
}

export function AboutVision() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isvisionLoading, setVisonLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setVisonLoading(true);
        try {
            const path = `/about-page`;
            const urlParamsObject = {
                populate: {
                    visionSection: { 
                        populate: "*",
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setVisonLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isvisionLoading) return "Loading";

    return (
        <AgenceVisionSection data={data}></AgenceVisionSection>
    );
}

export function AboutCTA() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/about-page`;
            const urlParamsObject = {
                populate: {
                    ctaSection: { populate: "*" },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <AgenceCTASection data={data}></AgenceCTASection>
    );
}

export function AboutTeam() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/about-page`;
            const urlParamsObject = {
                populate: {
                    EquipeSection: { 
                        populate: {
                            Membre: {
                                populate: "*",
                            }
                        }
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <AgenceTeamSection data={data}></AgenceTeamSection>
    );
}

export function AboutValeur() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/about-page`;
            const urlParamsObject = {
                populate: {
                    valeurSection: { 
                        populate: {
                            valeur: {
                                populate: "*",
                            }
                        }
                    },
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <AgenceValeurSection data={data}></AgenceValeurSection>
    );
}