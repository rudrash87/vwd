"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import Navigation from "@/components/header/navigation";
export default function TopmenuJson() {
  const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
  const options = { headers: { Authorization: `Bearer ${token}` } };

  const [data, setData] = useState<any>([]);
  const [topctadata, setTopCtaData] = useState<any>([]);
  const [isLoading, setLoading] = useState(true);


  const fetchData = useCallback(async (start: number, limit: number) => {
    setLoading(true);
    try {
      const path = `/top-menus`;
      const urlParamsObject = {
        sort: { id: "asc" },
        populate: {
            parentMenu: { populate: "*" },
            link: {populate: "*"},
            ico: {populate: "*"},
        },
      };
      const responseData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setData(responseData.data);
      } else {
        setData((prevData: any[]) => [...prevData, ...responseData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }

    setLoading(true);
    try {
      const path = `/top-ctas`;
      const urlParamsObject = {
        sort: { id: "asc" },
        populate: {
            link: { populate: "*" },
        },
      };
      const responseData = await fetchAPI(path, urlParamsObject, options);

      if (start === 0) {
        setTopCtaData(responseData.data);
      } else {
        setTopCtaData((prevData: any[]) => [...prevData, ...responseData.data]);
      }
    } catch (error) {
      console.error(error);
    } finally {
      setLoading(false);
    }
  }, []);


  useEffect(() => {
    fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
  }, [fetchData]);


  if (isLoading) return "Loading";

  return (
    <Navigation menudata={data} topcta={topctadata}></Navigation>
  );
}