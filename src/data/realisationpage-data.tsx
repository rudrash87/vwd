"use client";
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import RealisationPage from "@/app/realisationsubpages/realisationpage";

export default function Realisationdata() {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const options = { headers: { Authorization: `Bearer ${token}` } };

    const [data, setData] = useState<any>([]);
    const [isLoading, setLoading] = useState(true);


    const fetchData = useCallback(async (start: number, limit: number) => {
        setLoading(true);
        try {
            const path = `/projects`;
            const urlParamsObject = {
                sort: { releaseDate: "desc" },
                populate: {
                    media: {
                        populate: "*"
                    }
                },
            };
            const responseData = await fetchAPI(path, urlParamsObject, options);

            if (start === 0) {
                setData(responseData.data);
            } else {
                setData((prevData: any[]) => [...prevData, ...responseData.data]);
            }
        } catch (error) {
            console.error(error);
        } finally {
            setLoading(false);
        }
    }, []);


    useEffect(() => {
        fetchData(0, Number(process.env.NEXT_PUBLIC_PAGE_LIMIT));
    }, [fetchData]);


    if (isLoading) return "Loading";

    return (
        <RealisationPage data={data}></RealisationPage>
    );
}

