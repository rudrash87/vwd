"use client"
import { useState, useEffect } from "react"
import Layout from '@/app/layout'
import { fetchAPI } from "@/app/utils/fetch-api";
import { redirectToHomepage } from '@/app/utils/redirect';
import Head from 'next/head'
import ArticleBanner from "@/components/sections/article-banner";
import { BlogRecentData } from "@/data/blogpage-data";
import BasicText from "@/components/sections/basictext";
import { BottomCta } from "@/data/homepage-data";
import { getBlogs } from "@/data/faker-data";
import CTA from '@/components/ui/cta ';


export default function BlogDetail(Props:any) {
    const getblogdata = getBlogs(1);
    const articlebannerarray = [];
    const exhibitor = {
        id: Props.postData?.data[0]?.attributes?.id,
        cat: Props.postData?.data[0]?.attributes?.relatedCategories?.data[0]?.attributes.name,
        title: Props.postData?.data[0]?.attributes?.title,
        date: Props.postData?.data[0]?.attributes?.publicationDate,
        auth_image: Props.postData?.data[0]?.attributes?.author?.data?.attributes.profilePicture.data?.attributes.url,
        auth_image_alt: Props.postData?.data[0]?.attributes?.author?.data?.attributes.profilePicture.data?.attributes.alternativeText,
        written_by: Props.postData?.data[0]?.attributes?.author?.data?.attributes.firstName +" "+ Props.postData?.data[0]?.attributes?.author?.data?.attributes.lastName ,
        main_image: Props.postData?.data[0]?.attributes?.image.data?.attributes.url,
        main_image_alt: Props.postData?.data[0]?.attributes?.image.data?.attributes.alternativeText,
        slug: Props.postData?.data[0]?.attributes?.slug,
    };
    articlebannerarray.push(exhibitor);

    const ctadata = [
        {
            id: 1,
            bvariant: "isdark",
            label: "Commencer",
            url: "/",
            className: "dark-btn",
        },
        {
            id: 2,
            bvariant: "transparent",
            label: "Notre méthodologie",
            url: "/#",
            className: "trans-btn",
        }
    ];
    const ctaonedata = [
        {
            id: 1,
            bvariant: "isdark",
            label: "Télécharger",
            url: "/",
            className: "dark-btn",
        },
    ];
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
        <>
		 {domLoaded && ( <Layout>
		   <Head>
            <title>{Props.postData?.data[0]?.attributes?.title}</title>
           </Head>
           <div className="md:pt-16 pb-14 lg:pb-24">
                <div className=" sm:max-w-[576px] md:max-w-[768px] xl:max-w-[1200px] 2xl:max-w-[1300px] 4xl:max-w-[1546px] m-auto">
                    {domLoaded && (<ArticleBanner articledata={articlebannerarray} className=""></ArticleBanner>)}
                    
                </div>
            </div>

            <div>
                <div className="container">
                    <div className="flex flex-wrap">
                        <div className="w-full md:w-[70%] md:pr-10 lg:pr-20">{domLoaded && (<BasicText basicdata={Props.postData?.data[0]?.attributes?.content}></BasicText>)}</div>
                        <div className="w-full md:w-[30%]">
                            {/* <RecentArticle  articledata={Props.postData?.data[0]?.attributes?.relatedArticles?.data}></RecentArticle> */}
                            <BlogRecentData slug={Props.postData?.data[0]?.attributes?.relatedArticles?.data[0]?.attributes.slug}/>
                        </div>
                    </div>
                </div>
            </div>

            <section id="section4" className="text-center md:text-left mb-24 lg:mb-24 relative bg-backgroundDark lg:bg-transparent border-y lg:border-0 border-muted2 border-solid py-20">
                    <div className="container">
                        {domLoaded && (<CTA title="Notre ressource" Multicolor={"telechargeable"} btndata={ctaonedata} titlelevel={2} variant="half-w" className={"flex flex-wrap"}>
                        {"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}
                        </CTA>)}
                    </div>
                    <div className="z-[-1] absolute w-full md:w-[calc(100%-50px)] 2xl:w-[calc(100%-20px)] 3xxl:[calc(100%-50px)]  4xl:w-[calc(100%-165px)] h-full top-0 bottom-0 right-0 bg-backgroundDark border border-muted2 border-solid rounded-l-md"></div>
            </section>

             <BottomCta/>
		 </Layout> ) }
        </>
    )
}

export async function getServerSideProps(context:any) {
    // Fetch data from an API
    const { slug } = context.query;

    try {
        const postData = await getPostDetails(context.query.slug);
        if (postData.data.length === 0) {
            return redirectToHomepage();
        }


        return {
            props: { postData },
        };
    } catch (error) {
        return redirectToHomepage();
    }

}

async function getPostDetails(slug: string) {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const path = `/articles`;
    const urlParamsObject = {
        populate: {
            author: {
                populate: "*"
            },
            relatedCategories:{
                populate: "*"
            },
            image:{
                populate: "*"
            },
            content: {
                populate: {
                    media: {
                        populate: "*"
                    }
                }
            },
            relatedArticles: {
                pouplate: "*"
            }
        },
        filters: { slug },
    };
    const options = { headers: { Authorization: `Bearer ${token}` } };
    const response = await fetchAPI(path, urlParamsObject, options);
    return response;
}