"use client"
import { useState, useEffect } from "react"
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import Portfolio from "@/components/sections/portfolio";
import CTA from '@/components/ui/cta ';
import Imagetext from "@/components/sections/image-text";
import { getImageText, getPortfolioImages } from "@/data/faker-data";

export default function Technology(props:any) {
    const ImageTextData = getImageText(5);
    const portfoliodata = getPortfolioImages(7);
    const [domLoaded, setDomLoaded] = useState(false);
    const ctadata = [
        {
            id: 1,
            bvariant: "isdark",
            label: "Commencer",
            url: "/",
            className: "dark-btn",
        },
        {
            id: 2,
            bvariant: "transparent",
            label: "Notre méthodologie",
            url: "/#",
            className: "trans-btn",
        }
    ];
    const ctaonedata = [
        {
            id: 1,
            bvariant: "isdark",
            label: "Télécharger",
            url: "/",
            className: "dark-btn",
        },
    ];
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
        <>
            <div className="page-wrapper">
                <section id="section1" className="pt-16 md:pt-24">
                    <div className="container">
                        <div className="max-w-[912px] mx-auto text-center pb-9 pb-14">
                            <Title title={"UX design"} className=" pb-8 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy " level={1} Iswhite></Title>
                            <p className="text-[17px] md:text-[19px] tracking-[0.26px] md:tracking-[0.28px] leading-[1.4] md:leading-[1.6]  font-montbook ">{"L'étape d'UX design (User Experience), occupe une place centrale dans notre processus de création de sites internet. L'UX design consiste à élaborer des expériences interactives qui placent les utilisateurs au cœur de la réflexion. Chez Verywell Digital, nous mettons un point d'honneur à concevoir des expériences utilisateurs efficaces en nous appuyant sur des méthodes éprouvées."}</p>
                        </div>
                    </div>
                </section>
                <section id="section2" className="pt-16 md:pt-24">
                    <div className='overflow-hidden'>
                        {domLoaded && (<Imagetext className="text-image" textimage={ImageTextData}></Imagetext>)}
                    </div>
                </section>
                <section id="section3" className="md:py-[70px] xl:py-20 ">
                    <div className="container">
                        <div className="text-center pb-9 pb-14">
                            <div className="max-w-[612px] mx-auto">
                                <Title title={"Quelques exemples"} className=" pb-8 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy " level={2} Iswhite></Title>
                                <div className="pb-14"><p className="text-lg font-montbook">{"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempo."}</p></div>
                            </div>
                            {domLoaded && (<Portfolio className=" p-1.5 pb-14" portfoliodata={portfoliodata} variant="technology"></Portfolio>)}
                            <Button variant="isdark" label={"Voir toutes nos réalisations"} href={"/"}></Button>
                        </div>
                    </div>
                </section>
                <section id="section4" className="text-center md:text-left mb-24 lg:mb-24 relative bg-backgroundDark lg:bg-transparent border-y lg:border-0 border-muted2 border-solid py-20">
                    <div className="container">
                        {domLoaded && (<CTA Multicolor={"Rejoignez-nous"} btndata={ctadata} titlelevel={2} variant="half-w" className={"flex flex-wrap"}>
                        {"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}
                        </CTA>)}
                    </div>
                </section>
                <section id="section5" className="text-center py-24 xl:py-36 2xl:py-48 3xl:py-56 relative before:absolute before:content-[''] before:rotate-180 before:top-0 before:left-0 before:rotate-[-360deg] before:h-full before:w-[250px] xl:before:w-[280px] 2xl:before:w-[310px] 3xl:before:w-[345px] before:bg-cover before:bg-[url('../../public/images/left-side.png')] before:opacity-90 after:absolute after:content-[''] after:top-0 after:right-0 after:h-full after:w-[350px] xl:after:w-[400px] 2xl:after:w-[450px] 3xl:after:w-[560px]] after:bg-cover after:bg-[url('../../public/images/right-side.png')] after:opacity-80 before:hidden lg:before:block after:hidden lg:after:block after:bg-center before:bg-center after:bg-cover before:bg-cover after:bg-no-repeat before:bg-no-repeat">
                    <div className="container">
                        {domLoaded && (<CTA Multicolor={"Démarrez votre project"} btndata={ctadata} titlelevel={2} variant="full-w" className={"multicolor-title"}>
                            {"C'est le début d'une belle aventure ! Planifiez une visio avec un de nos experts."}
                        </CTA>)}
                    </div>
                </section>
            </div>
        </>
    )
}

