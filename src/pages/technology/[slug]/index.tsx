"use client"
import { useState, useEffect } from "react"
import Layout from '@/app/layout'
import Head from 'next/head'
import Technology from './technology'


export default function Tarification(props:any) {
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
        <>
		 {domLoaded && ( <Layout>
		   <Head>
            <title>{props.title}</title>
           </Head>
           <Technology />
		 </Layout> ) }
        </>
    )
}

type Params = {
    params: {
        slug: string
    }
}

export async function getStaticPaths() {
    return {
        paths: [{ params: { slug: 'ux-design' } }],
        fallback: true,
    }
}

export async function getStaticProps({ params }: Params) {
    // Pass post data to the page via props
	return {
        props: { 
		  title: params.slug,
          content: "Hello there" 
		
		},
		    revalidate: 60,
    }
}