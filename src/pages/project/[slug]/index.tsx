"use client"
import { useState, useEffect } from "react"
import Layout from '@/app/layout'
import Head from 'next/head'
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import Portfolio from "@/components/sections/portfolio";
import { getPortfolioImages, getTechdescriptiondata, getReviewdata } from "@/data/faker-data";
import Techdescription from "@/components/sections/techdescription";
import Customerreivew from "@/components/sections/customer-review";


export default function BlogDetail(props:any) {
    const detailimage = getPortfolioImages(4);
    const techdescriptiondata = getTechdescriptiondata(4);
    const reviewdata = getReviewdata(1);
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
        <>
		 {domLoaded && ( <Layout>
		   <Head>
            <title>{props.title}</title>
           </Head>
           <div className="">
        <div className="py-16 lg:py-24">
          <section id="section1" className="container">
            {domLoaded && (<Portfolio className="flex flex-wrap -mx-2.5" portfoliodata={detailimage} variant="projectdetails"></Portfolio>)}
          </section>
          <section className="container">
            <div className="flex-wrap flex">
                <div className="w-full text-info text-lg py-6 sm:py-12" onClick={() => window.history.back()}>{"< RETOUR"}</div>
                <div className="flex-wrap flex w-full md:items-center">
                  <Title title={"Sunny Side of the Doc"} className="pb-4 md:pb-0 md:pr-12 block md:inline-block trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" level={1} Iswhite isHTML></Title>
                  <Button variant="isdark" label={"Voir le site"} href={"/"}></Button>
                </div>
                <p className="max-w-[830px] leading-7 md:leading-9 text-base md:text-lg w-full pt-5">{"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."}</p>
            </div>
          </section>
          <section className="container">
            <div className="py-16 lg:py-28 xl:py-32 2xl:py-40">
              <Title title={"Description technique"} className="pb-7 text-center md:text-left text-2xl lg:text-3xl xl:text-4xl" level={3} Iswhite></Title>
              {domLoaded && (<Techdescription techdescriptiondata={techdescriptiondata}></Techdescription>)}
            </div>
          </section>
          <section className="container">
              <Title title={"Avis client"} className="pb-7" level={3} Iswhite></Title>
              {domLoaded && (<Customerreivew className="review-item" reviewdata={reviewdata}></Customerreivew>)}
          </section>
        </div>
      </div>
		 </Layout> ) }
        </>
    )
}

type Params = {
    params: {
        slug: string
    }
}

export async function getStaticPaths() {
    return {
        paths: [{ params: { slug: 'ux-design' } }],
        fallback: true,
    }
}

export async function getStaticProps({ params }: Params) {
    // Pass post data to the page via props
	return {
        props: { 
		  title: params.slug,
          content: "Hello there" 
		
		},
		revalidate: 60,
    }
}