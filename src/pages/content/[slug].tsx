"use client"
import Layout from '@/app/layout'
import Head from 'next/head'
import { useState, useEffect, useCallback } from "react";
import { fetchAPI } from "@/app/utils/fetch-api";
import Title from "@/components/ui/title";
import BasicText from "@/components/sections/basictext";
import { redirectToHomepage } from '@/app/utils/redirect';

export default function Tarification(Props:any) {
    const [domLoaded, setDomLoaded] = useState(false);

    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
        <>
            {domLoaded && (<Layout>
                <Head>
                    <title>{Props.postData?.data[0]?.attributes?.title}</title>
                </Head>
                <section id="section1" className="container">
                    <div className='pt-16 xl:pt-24 2xl:pt-28 3xl:pt-32 max-w-[960px] mx-auto'>
                        <Title title={Props.postData?.data[0]?.attributes?.title} className="text-center trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" level={1} Iswhite></Title>
                    </div>
                </section>
                <section id="section2" className="container">
                    <div className='max-w-[960px] mx-auto pb-20 xl:pb-24 2xl:pb-28 3xl:pb-32 pt-14'>
                        <BasicText basicdata={Props.postData?.data[0]?.attributes?.content}></BasicText>
                    </div>
                </section>
            </Layout>)}
        </>
    )
}

export async function getServerSideProps(context:any) {
    // Fetch data from an API
    const { slug } = context.query;

    try {
        const postData = await getPostDetails(context.query.slug);
        if (postData.data.length === 0) {
            return redirectToHomepage();
        }


        return {
            props: { postData },
        };
    } catch (error) {
        return redirectToHomepage();
    }

}

async function getPostDetails(slug: string) {
    const token = process.env.NEXT_PUBLIC_STRAPI_API_TOKEN;
    const path = `/Pages`;
    const urlParamsObject = {
        populate: "*",
        filters: { slug },
    };
    const options = { headers: { Authorization: `Bearer ${token}` } };
    const response = await fetchAPI(path, urlParamsObject, options);
    return response;
}