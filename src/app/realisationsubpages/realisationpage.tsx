import Title from "@/components/ui/title";
import Portfolio from "@/components/sections/portfolio";

export default function RealisationPage(Props:any) {
    const realisationdata = Props.data;
    return (
      <>  
        <div className="">
            <div className="py-16 xl:py-20 2xl:py-24">
                <section className="">
                <div className="container">
                    <Title title={"Nos réalisations"} className="text-center sm:text-left pb-9 xl:pb-11 3xl:pb-14 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" level={1} Iswhite></Title>
                </div>
                <Portfolio className="flex flex-wrap p-1.5" portfoliodata={realisationdata} variant="realisations"></Portfolio>
                </section>
            </div>
        </div>
    </>
    )
}