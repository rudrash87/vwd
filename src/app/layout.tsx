"use client"
import "@/styles/globals.css";
import "@/styles/swiper.css";
import Header from "@/components/header/header";
import FooterJson from "@/data/footer-fetch";
// import Footer from "@/components/footer";
import { montblack, montblackitalic, montbold, montbolditalic, montbook, montbookitalic, montextralight, montextralightitalic, monthairline, monthairlineitalic, montheavy, montheavyitalic, montlight, montlightitalic, montregular, montregularitalic, montsemibold, montsemibolditalic, montthin, montthinitalic } from "@/styles/fonts";

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="fr">
      <body className={`${montblack.variable}
      ${montblackitalic.variable}
      ${montbold.variable}
      ${montbolditalic.variable}
      ${montbook.variable}
      ${montbookitalic.variable}
      ${montextralight.variable}
      ${montextralightitalic.variable}
      ${monthairline.variable}
      ${monthairlineitalic.variable}
      ${montheavy.variable}
      ${montheavyitalic.variable}
      ${montlight.variable}
      ${montlightitalic.variable}
      ${montregular.variable}
      ${montregularitalic.variable}
      ${montsemibold.variable}
      ${montsemibolditalic.variable}
      ${montthin.variable}
      ${montthinitalic.variable}
      `}>
        <header className='font-montblack relative z-50'>
          <div className='max-w-[1920px] mx-auto px-4 2xl:px-8 py-4'>
            <Header/>
          </div>
        </header>
        <main>
          <div className=''>
            {children}
          </div>
        </main>
        <footer>
            <FooterJson />
        </footer>
      </body>
    </html>
  )
}
