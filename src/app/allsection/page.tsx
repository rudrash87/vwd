"use client"
import { useState, useEffect } from "react"
import Title from "@/components/ui/title";
import Icons from "@/components/sections/icons";
import CTA from "@/components/ui/cta ";
import Imagetext from "@/components/sections/image-text";
import TableData from "@/components/sections/table";
import Feature from "@/components/sections/feature";
import Pricecard from "@/components/sections/pricecard";
import Pagination from "@/components/ui/Pagination";
import { getAccordion, getSwiperData, getImageText, getPriceCard, getImagesData } from "@/data/faker-data";

export default function Faq() {
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    const pricecard = getPriceCard(3);
    const icons = getImagesData(3);
    const iconssix = getImagesData(6);
    const featuredata = getImageText(3);
    const featureleftdata = getImageText(6);
    const grid2data = getImageText(4);

    const section1data = {
        title: "Lorem ipsum dolor",
        multicolor: "",
        level: 2,
        className: "banner-cta",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 1,
                bvariant: "isdark",
                label: "Commencer",
                url: "/",
                className: "dark-btn",
            },
            {
                id: 2,
                bvariant: "transparent",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const sectionlastdata = {
        btndata: [
            {
                id: 1,
                bvariant: "isdark",
                label: "Prendre rendez-vous",
                url: "/",
                className: "dark-btn",
            },
            {
                id: 2,
                bvariant: "transparent",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const sectionwithouttitlecta = {
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 2,
                bvariant: "transparent",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const sectionwithtitlecta = {
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 2,
                bvariant: "isdark",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const onPageChange ="";

    const sectionwithtitlectadark = {
        title: "Lorem ipsum dolor",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 2,
                bvariant: "isdark",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const sectionctafulldata = {
        level: 2,
        className: "banner-cta",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 1,
                bvariant: "isdark",
                label: "Commencer",
                url: "/",
                className: "dark-btn",
            },
            {
                id: 2,
                bvariant: "transparent",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const ImageTextArray = [];
    const ImageTextData = {
        link: "",
        image: "https://loremflickr.com/320/240",
        title: "Lorem ipsum dolor",
        level: 2,
        multicolor: "",
        toptitle: "ÉTAPE 1",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        position: "",
    };
    ImageTextArray.push(ImageTextData);

    const section3data = {
        title: "Lorem ipsum dolor sit amet consecute",
        multicolor: "",
        toptitle: "ÉTAPE 2",
        level: 2,
        className: "banner-cta",
        text: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        btndata: [
            {
                id: 1,
                bvariant: "isdark",
                label: "Commencer",
                url: "/",
                className: "dark-btn",
            },
            {
                id: 2,
                bvariant: "transparent",
                label: "Lorem ipsum",
                url: "/#",
                className: "trans-btn",
            }
        ]
    };

    const tabledata = {
        btndata: [
            {
                id: 1,
                label: "2 cores",
                monthly: "60 hour free",
                hourly: "$0.18",
            },
            {
                id: 2,
                label: "4 cores",
                monthly: "30 hour free",
                hourly: "$0.36",
            },
            {
                id: 3,
                label: "8 cores",
                monthly: "15 hour free",
                hourly: "$0.72",
            },
            {
                id: 4,
                label: "16+ cores",
                monthly: "N/A",
                hourly: "See pricing docs",
            },
            {
                id: 5,
                label: "Storage",
                monthly: "15 GB free",
                hourly: "$0.07",
            }
        ]
    };

    const Section5Array = [];
    const Section5Data = {
        link: "",
        image: "https://loremflickr.com/320/240",
        title: "Recette et livrables",
        level: 2,
        multicolor: "",
        toptitle: "ÉTAPE 3",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        position: "",
    };
    Section5Array.push(Section5Data);


    return (
        <>
            <section className="text-center md:text-left pt-24 xl:pt-28 2xl:pt-36 3xl:pt-44">
                <div className="container">
                    {domLoaded && (<CTA title={section1data.title} btndata={section1data.btndata} Multicolor={section1data.multicolor}  titlelevel={2} variant="70-w" className={section1data.className}>{section1data.text}</CTA>)}
                </div>
            </section>
            <section className="section2 overflow-hidden">
                {domLoaded && (<Imagetext className="text-image" textimage={ImageTextArray} icons={icons}></Imagetext>)}
            </section>
            <section className="section3 text-center">
                <div className="container">
                    {domLoaded && (<CTA title={section3data.title} Multicolor={section3data.multicolor} toptitle={section3data.toptitle} btndata={section3data.btndata} titlelevel={3} variant="full-w" className={section3data.className}>{section3data.text}</CTA>)}
                    <div className="text-center md:text-left mt-16 xl:mt-20 2xl:mt-24 max-w-[966px] mx-auto bg-muted6 rounded-md pl-10 pt-10 pr-10 lg:pl-12 lg:pt-12 lg:pr-12 last-bg overflow-hidden">
                        {domLoaded && (<TableData tabledata={tabledata.btndata} className="tables" title={"hello"} level={4} text={"hello"}></  TableData>)}
                    </div>
                </div>
            </section>
            <section className="section5 overflow-hidden">
                {domLoaded && (<Imagetext className="text-image"  textimage={Section5Array}></Imagetext>)}
            </section>

            <div className="container">
                <Title title={"Lorem ipsum dolor sit amet consecute"} level={3} Iswhite isHTML className="text-center max-w-[386px] mx-auto pb-5 md:pb-9"></Title>
                {domLoaded && (<Feature varient={"grid3-center"} className="" featuredata={featuredata}></Feature>)}
            </div>

            <section className="section5 overflow-hidden">
                {domLoaded && (<Imagetext className="text-image"  textimage={Section5Array}></Imagetext>)}
            </section>

            <div className="container">
                <Title title={"Lorem ipsum dolor sit amet consecute"} level={3} Iswhite isHTML className="text-center max-w-[386px] mx-auto pb-5 md:pb-9"></Title>
                {domLoaded && (<Feature varient={"grid3-left"} className="" featuredata={featureleftdata}></Feature>)}
            </div>

            <div className="container">
                <div className="text-center pt-20 lg-pt-36 xl:pt-48 2xl:pt-60 3xl:pt-72 pb-20 lg:pb-24 xl:pb-36 2xl:pb-44 3xl:pb-52">
                    {domLoaded && (<CTA title={sectionwithtitlectadark.title} btndata={sectionwithtitlectadark.btndata} titlelevel={3} variant="full-w">{sectionwithtitlectadark.text}</CTA>)}
                    <div className="text-center md:text-left mt-9 xl:mt-10 2xl:mt-12 mx-auto bg-muted6 rounded-md pl-10 pt-10 pr-10 lg:pl-12 lg:pt-12 lg:pr-12 last-bg overflow-hidden">
                        {domLoaded && (<TableData tabledata={tabledata.btndata} className="tables" title={"hello"} level={4} text={"hello"}></  TableData>)}
                    </div>
                    <div className="pt-6">
                        {domLoaded && (<Icons textimage={iconssix} tvarient={"6gird"} className="6grid"></Icons>)}
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="text-center pb-20 lg:pb-24 xl:pb-36 2xl:pb-44 3xl:pb-52">
                    {domLoaded && (<CTA btndata={sectionwithouttitlecta.btndata} titlelevel={3} variant="full-w">{sectionwithouttitlecta.text}</CTA>)}
                </div>
            </div>

            <section id="section2" className="py-20 xl:py-28 2xl:py-32 3xl:py-36">
                <div className="container">
                    <div className="max-w-[973px] mx-auto">
                        <div className="mx-auto pb-4 max-w-[580px] text-center">
                            <Title title={"Lorem ipsum dolor"} level={3} Iswhite isHTML className="text-center mx-auto pb-5"></Title>
                            <p className="text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                        <div id="section2" className="pb-20">
                        <div className="flex flex-wrap justify-center">
                            {domLoaded && (<Pricecard varient="2card-left" pricecard={pricecard} className="price-item"></Pricecard>)}
                        </div>
                    </div>
            </div>
                </div>
            </section>

            <div className="container">
                <div className="mx-auto max-w-[580px] text-center">
                    <Title title={"Lorem ipsum dolor"} level={3} Iswhite isHTML className="text-center mx-auto pb-5"></Title>
                    <p className="text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                </div>
                {domLoaded && (<Feature varient={"grid2-icon"} className="" featuredata={grid2data}></Feature>)}
            </div>

            <section id="section2" className="py-20 xl:py-28 2xl:py-32 3xl:py-36">
                <div className="container">
                    <div className="mx-auto pb-4 max-w-[580px] text-center">
                        <Title title={"Lorem ipsum dolor"} level={3} Iswhite isHTML className="text-center mx-auto pb-5"></Title>
                        <p className="text-success">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                    </div>
                    <div className="flex flex-wrap justify-center max-md:-mx-2.5 md:-mx-7 lg:-mx-8">
                        {domLoaded && (<Pricecard varient="3card" pricecard={pricecard} className="price-item"></Pricecard>)}
                    </div>
                </div>
            </section>

            <div className="container py-20 xl:py-28 2xl:py-32 3xl:py-36">
                <div className="text-center max-w-[1144px] mx-auto bg-background border border-muted2 border-solid rounded-md  shadow-[0_0px_80px_rgb(71,32,80,1)] py-20 xl:py-28 2xl:py-32 px-5">
                    <div className="max-w-[600px] mx-auto">
                        {/* last section cta */}
                        {domLoaded && (<CTA title={"Planifiez votre"} btndata={sectionlastdata.btndata} Multicolor={"rendez-vous"} titlelevel={2} variant="box-pack" className="border-cta"></CTA>)}
                    </div>
                </div>
            </div>

            <div className="container">
                <div className="pb-16 lg:pb-20 xl:pb-24">
                    {/* <Pagination
                        items={10} // 100
                        currentPage={1} // 1
                        pageSize={5} // 10
                        onPageChange={onPageChange}
                    /> */}
                </div>
            </div>
        </>
    )
}