"use client"
import { useState, useEffect } from "react"
import Title from "@/components/ui/title";
import { getSwiperData} from "@/data/faker-data";
import SwiperItem from "@/components/sections/swiper-section";
import Tarrificationbannerdata from "@/data/tarrificationpage-data";
import { Tarrificationfaqdata, Tarrificationaddondata, Tarrificationpricedata } from "@/data/tarrificationpage-data";

export default function Tarification () {
    const swiperdata = getSwiperData(5);
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);
    return (
      <>
        <div className="page-wrapper">
            <Tarrificationbannerdata />
            <Tarrificationpricedata />
            <Tarrificationaddondata />
            
            <section id="section4" className="mb-40 py-20 md:py-24 relative">
                <div className="container">
                    <Title title={"Des clients satisfaits"} level={3} Iswhite className="center font-montbold text-xl md:text-2xl lg:text-3xl xl:text-4xl text-white pb-5 md:pb-9"></Title>
                    {domLoaded && (<SwiperItem slidesView={1} navigation isclient autoplay spacebetween={20} className="swiper-client" position={"default"} isHTML swiperdata={swiperdata}></SwiperItem>)}
                </div>
                <div className="z-[-1] absolute w-full md:w-[calc(100%-50px)] 2xl:w-[calc(100%-20px)] 3xxl:w-[calc(100%-50px)]  4xl:w-[calc(100%-165px)] h-full top-0 bottom-0 right-0 bg-backgroundDark border border-muted2 border-solid rounded-l-md"></div>
            </section>

            <Tarrificationfaqdata />
        </div>
      </>
    )
}