"use client"
import Aboutbanner from "@/data/aboutpage-data";
import { AboutCTA, AboutValeur, AboutTeam, AboutVision } from "@/data/aboutpage-data";

export default function Tarification () {
    return (
      <>
        <div className="w-full">
            <Aboutbanner />
            <AboutVision />
            <AboutValeur />
            <AboutTeam />
            <AboutCTA />
        </div>
      </>
    )
}