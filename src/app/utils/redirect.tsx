export function redirectToHomepage() {
    return {
      redirect: {
        destination: `/`,
        permanent: false,
      },
    };
  }