"use client"
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import Imagetext from "@/components/sections/image-text";
import FilterBlog from "@/components/sections/filter-blog";
import Loadmoreimage from "@/components/sections/loadmore-image";
import Icontext from "@/components/sections/icon-text";
import CTA from "@/components/ui/cta ";
import ArticleBanner from "@/components/sections/article-banner";
import Portfolio from "@/components/sections/portfolio";
import FaqAccordion from "@/components/sections/faq-accordion";
import SwiperItem from "@/components/sections/swiper-section";
import { useState, useEffect } from "react"
import { getAccordion, getSwiperData, getImageText, getMoreimages, getBlogs, getPortfolioImages, getArticelBannerdata } from "@/data/faker-data";

export default function Faq() {
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);

    const faqdata = getAccordion(10);
    const swiperdata = getSwiperData(4);
    const imagetext = getImageText(1);
    const icontext = getImageText(6);
    const loadmoredata = getMoreimages(10);
    const blogsdata = getBlogs(10);
    const btndata = getMoreimages(2);
    const portfoliodata = getPortfolioImages(7);
    const articlebanner = getArticelBannerdata(1);
    return (
        <>
            <section className="page-title">
                <Title title="Faq" Multicolor="test" className="uppercase" level={1} isHTML Iswhite></Title>
            </section>
            <section className="faq-section">
                {domLoaded && (<FaqAccordion className="accordion" faqs={faqdata}></FaqAccordion>)}
            </section>
            <section className="swiper-section">
                <Title title="Swiper Section" className="uppercase text-3xl font-bold" level={2}></Title>
                <div className="swiper-section-wrapper">
                    {domLoaded && (<SwiperItem slidesView={2} navigation autoplay iscard spacebetween={20} className="swiper-faq" position={"default"} isHTML swiperdata={swiperdata}></SwiperItem>)}
                </div>
            </section>
            <section className="button-wrapper">
                <Title title="Button Section" className="uppercase text-3xl font-bold" level={2}></Title>
                <Button className="link-two" label="Link" href="/" variant="isdark"></Button>
            </section>
            <section className="text-image-outer">
                <Title title="Text Image Section" className="uppercase text-3xl font-bold" level={2}></Title>
                {domLoaded && (<Imagetext className="text-image" textimage={imagetext}></Imagetext>)}
            </section>
            <section className="text-icon-outer">
                <Title title="Les technos" className="uppercase text-3xl font-montbold" level={2}></Title>
                {domLoaded && (<Icontext className="text-image" textimage={icontext}></Icontext>)}
                <Button className="link-two text-center" label="Commencer" href="/" variant="isdark"></Button>
            </section>
            <section className="text-icon-outer">
                <Title title="Load more section" className="uppercase text-3xl font-bold" level={2}></Title>
                {domLoaded && (<Loadmoreimage initialimte={4} loadlimit={4} className="loadmoredata" imagelinkdata={loadmoredata}></Loadmoreimage>)}
            </section>
            <section className="blog-wrapper">
                {domLoaded && (<FilterBlog className="blog-outer" blogdata={blogsdata}></FilterBlog>)}
            </section>
            {/* <section className="cta-wrapper">
                <Title title="CTA" className="uppercase text-3xl font-bold" level={2}></Title>
                {domLoaded && (<CTA title="Lorem Ipsum" btndata={btndata} Multicolor="test" titlelevel={4} variant="full-w" className="cta-full-wrapper">
                    {"C'est le début d'une belle aventure ! Planifiez une visio avec un de nos experts."}
                </CTA>)}

                {domLoaded && (<CTA title="Lorem Ipsum 50-50" btndata={btndata} Multicolor="test" titlelevel={4} variant="half-w" className="cta-half-wrapper">
                    {"C'est le début d'une belle aventure ! Planifiez une visio avec un de nos experts."}
                </CTA>)}
            </section> */}
            <section className="portfolio-section">
                <Title title="Portfolio" className="uppercase text-3xl font-bold" level={2}></Title>
                {domLoaded && (<Portfolio className="portfolio-wraper" portfoliodata={portfoliodata} variant="technology"></Portfolio>)}
            </section>
            <section className="article-banner">
                {domLoaded && (<ArticleBanner className="article-banner" articledata={articlebanner}></ArticleBanner>)}
            </section>
        </>
    )
}