import Visioncard from "@/components/sections/vision";
export default function AgenceVisionSection(Props:any) {
    const visiondata = Props.data;
    return (
        <>
            <section id="section2" className="container">
                <Visioncard className="flex flex-wrap items-center" visiondata={visiondata.attributes.visionSection}></Visioncard>
            </section>
        </>
    )
}