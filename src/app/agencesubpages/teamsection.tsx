"use client"
import Title from "@/components/ui/title";
import Teamcard from "@/components/sections/teamcard";

export default function AgenceTeamSection(Props:any) {
  const teamdata = Props.data;
  return (
    <>
        <section id="section4" className="w-full">
            <div className="container">
                <Title title={teamdata.attributes.EquipeSection.title} className="text-center pb-9 font-montbold text-xl md:text-2xl lg:text-3xl xl:text-4xl" level={3} Iswhite></Title>
            </div>
            <div className="px-5 xl:px-12 3xl:px-24">
                <Teamcard className="w-full pb-16 xl:pb-2 sm:w-1/2 md:w-1/3 lg:w-1/4 xl:w-1/5 sm:pt-2 sm:pl-2 sm:pr-2" teamcarddata={teamdata.attributes.EquipeSection.Membre}></Teamcard>
            </div>
        </section>
        <section id="section5" className="container">
            <div className="pb-20 pt-16 lg:py-32">
                <p className="leading-9 2xl:leading-10 tracking-[0.34px] 2xl:tracking-[0.04px] text-xl 2xl:text-2xl mx-auto text-center max-w-[944px]">{teamdata.attributes.EquipeSection.description}</p>
            </div>
        </section>
    </>
  )
}
