import Title from "@/components/ui/title";
import Feature from "@/components/sections/feature";
export default function AgenceValeurSection(Props:any) {
    const valeurdata = Props.data;
    return (
        <>
            <section id="section3" className="container">
                <div className="py-16 md:py-24 xl:py-28 2xl:py-32 3xl:py-36 text-center md:text-left">
                    <Title title={valeurdata.attributes.valeurSection.title} className="pb-2 font-montbold text-xl md:text-2xl lg:text-3xl xl:text-4xl" level={3} Iswhite></Title>
                    <Feature className="" varient={"grid3"} featuredata={valeurdata.attributes.valeurSection.valeur}></Feature>
                </div>
            </section>
        </>
    )
}