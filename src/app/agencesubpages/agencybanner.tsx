"use client"
import Title from "@/components/ui/title";

export default function AgencebannerSection(Props:any) {
  const bannerdata = Props.data;
  return (
    <>
        <section id="section1" className="py-16 md:py-24 text-center"> 
            <div className="container">
                <Title title={bannerdata.attributes.title} className="pb-6 font-montheavy xl:pb-8 tracking-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px]" level={1} Iswhite></Title>
                <p className="max-w-[910px] mx-auto">{bannerdata.attributes.description}</p>
            </div>
        </section>
    </>
  )
}
