"use client"
import CTA from "@/components/ui/cta ";

export default function AgenceCTASection(Props:any) {
  const ctaarray = Props.data;
  const ctadata = [
    {
      id: 1,
      bvariant: "isdark",
      label: ctaarray.attributes.ctaSection.linkText,
      url: ctaarray.attributes.ctaSection.linkUrl,
      className: "dark-btn",
      newwindow: true,
    },
];
  return (
    <>
        <section id="section6" className='text-center md:text-left mb-24 lg:mb-24 relative bg-backgroundDark lg:bg-transparent border-y lg:border-0 border-muted2 border-solid py-20'>
            <div className="container">
                <CTA Multicolor={ctaarray.attributes.ctaSection.title} btndata={ctadata} titlelevel={2} variant="half-w" className={"flex flex-wrap"}>
                {ctaarray.attributes.ctaSection.description}
                </CTA>
            </div>
            <div className="z-[-1] absolute w-full md:w-[calc(100%-50px)] 2xl:w-[calc(100%-20px)] 3xxl:w-[calc(100%-50px)]  4xl:w-[calc(100%-165px)] h-full top-0 bottom-0 right-0 bg-backgroundDark border border-muted2 border-solid rounded-l-md"></div>
        </section>
    </>
  )
}
