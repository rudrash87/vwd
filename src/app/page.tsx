"use client"
import TechSection from './Homesubpages/TechSection';
import RecentProject from './Homesubpages/RecentProject';
import ArticleRecent from './Homesubpages/ArticleRecent';
import { getImageText, getHomeImageText, getSwiperData } from "@/data/faker-data";
import { useState, useEffect } from "react"
import Homebanner from '@/data/homepage-data';
import { BottomCta, ProcessSectionData, TechSectionData, ProjectSectionData, ArticleSectionData } from '@/data/homepage-data';

export default function Home() {

  const icontextbtn = {
    id: 1,
    bvariant: "isdark",
    label: "Commencer",
    url: "/",
    className: "text-center",
  }

  const projectrecentbtn = {
    id: 1,
    bvariant: "isdark",
    label: "Voir toutes nos réalisationsnp",
    url: "/",
    className: "mt-10 md:mt-16 text-center",
  }

  const articlerecentbtn = {
    id: 1,
    bvariant: "isdark",
    label: "Voir toutes nos réalisationsnp",
    url: "/",
    className: "mt-10 md:mt-16 text-center",
  }

  const ctadata = [
    {
      id: 1,
      bvariant: "isdark",
      label: "Commencer",
      url: "/",
      className: "dark-btn",
    },
    {
      id:2,
      bvariant: "transparent",
      label: "Notre méthodologie",
      url: "/#",
      className: "trans-btn",
    }
  ];
  const ImageTextData = getHomeImageText(5);
  const icontext = getImageText(6);
  const [domLoaded, setDomLoaded] = useState(false);
  const swiperdata = getSwiperData(5);
  useEffect(() => {
      setDomLoaded(true);
  }, []);
  return (
    <>
      <Homebanner />
      <ProcessSectionData />
      <TechSectionData />
      <ProjectSectionData />
      <ArticleSectionData />
      <BottomCta />
    </>
  )
}
