"use client"
import Title from "@/components/ui/title";
export default function BlogbannerSection(Props:any) {
    const bannerdata = Props.data;
    return (
        <>
            <div className="flex flex-wrap pb-8 md:pb-10 xl:pb-[70px] md:-mx-2.5 items-center max-md:px-2.5 ">
                <div className="w-full md:w-[35%] max-md:pb-7 md:px-2.5"><Title title={bannerdata.attributes.title} className="uppercase trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy " level={2} Iswhite isHTML></Title></div>
                <div className="w-full md:w-[65%] md:px-2.5"><p className="text-[19px] tracking-[1.3] md:tracking-[0.28px] leading-[1.4] md:leading-[1.6]  font-montbook ">{bannerdata.attributes.description}</p></div>
            </div>
        </>
    )
}