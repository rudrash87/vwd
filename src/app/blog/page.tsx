"use client"
import Blogbanner from "@/data/blogpage-data";
import FilterBlog from "@/components/sections/filter-blog";
import { useState, useEffect } from "react"
import { getBlogs } from "@/data/faker-data";

export default function Blogs() {
    const blogsdata = getBlogs(10);
    const [domLoaded, setDomLoaded] = useState(false);
    useEffect(() => {
        setDomLoaded(true);
    }, []);

    return (
        <>
            <section className=" pt-20 pb-[70px] ">
                <div className="container">
                    <Blogbanner />
                    {domLoaded && (<FilterBlog className="" blogdata={blogsdata}></FilterBlog>)}
                </div>
            </section>
        </>
    )
}