"use client"
import CTA from "@/components/ui/cta ";

export default function HomeCta(Props: any) {
    const ctadata = Props.ctadata;
    const btndata = [
        {
            id: ctadata.attributes.ctaSection.button1.id,
            bvariant: "isdark",
            label: ctadata.attributes.ctaSection.button1.text,
            url: ctadata.attributes.ctaSection.button1.url,
            className: "dark-btn",
            newwindow: ctadata.attributes.ctaSection.button1.newWindow,
        },
        {
            id: ctadata.attributes.ctaSection.button2.id,
            bvariant: "transparent",
            label: ctadata.attributes.ctaSection.button2.text,
            url: ctadata.attributes.ctaSection.button2.url,
            className: "trans-btn",
            newwindow: ctadata.attributes.ctaSection.button2.newWindow,
        }
    ];
    return (
        <>
            <section id="section6" className="text-center py-24 xl:py-36 2xl:py-48 3xl:py-56 relative before:absolute before:content-[''] before:rotate-180 before:top-0 before:left-0 before:rotate-[-360deg]  before:h-full before:w-[250px] xl:before:w-[280px] 2xl:before:w-[310px] 3xl:before:w-[345px] before:bg-cover before:bg-[url('../../public/images/left-side.png')] before:opacity-90 after:absolute after:content-[''] after:top-0 after:right-0 after:h-full after:w-[350px] xl:after:w-[400px] 2xl:after:w-[450px] 3xl:after:w-[560px] after:bg-cover after:bg-[url('../../public/images/right-side.png')] after:opacity-80 before:hidden lg:before:block after:hidden lg:after:block after:bg-center before:bg-center after:bg-cover before:bg-cover after:bg-no-repeat before:bg-no-repeat">
                <div className="container">
                    <CTA Multicolor={ctadata.attributes.ctaSection.title} btndata={btndata} titlelevel={2} variant="full-w" className={"multicolor-title"}>
                        {ctadata.attributes.ctaSection.description &&
                            <>
                                {ctadata.attributes.ctaSection.description}
                            </>
                        }
                    </CTA>
                </div>
            </section>
        </>
    )
}