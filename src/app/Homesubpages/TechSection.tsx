"use client"
import Title from '@/components/ui/title';
import Icontext from '@/components/sections/icon-text';
import Button from '@/components/ui/button';

export default function TechSection(Props:any) {

  const techdata = Props.data;
  const imagedata = techdata.attributes.techSection.technology;
  return (
    <>
      <section id="section3" className='pt-24'>
        <div className="container">
          <div className='text-center max-w-lg m-auto pb-9'>
            <Title className="font-montheavy pb-2.5 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" title={techdata.attributes.techSection.title} level={2} isHTML Iswhite></Title>
            {techdata.attributes.techSection.description && (
              <p className="text-[17px] 2xl:text-[19px] leading-[1.4] 2xl:leading-[1.84] font-montbook trakcing-[0.26px] 2xl:tracking-[0.26px]">{techdata.attributes.techSection.description}</p>
            )}
          </div>
          <div className=' flex flex-wrap sm:-mx-3 pb-8 pb-12'>
            <Icontext className="text-image" textimage={imagedata}></Icontext>
          </div>

          <div className="text-center">
            <Button className={"text-center"} newwindow={techdata.attributes.techSection.link.newWindow} label={techdata.attributes.techSection.link.text} href={techdata.attributes.techSection.link.url} variant={"isdark"}></Button>
          </div>
        </div>
      </section>
    </>
  )
}
