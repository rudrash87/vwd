"use client"
import HomeBanner from '@/components/banner/home-banner';
import Scrollto from '@/components/ui/scrollto';

export default function HomeBannerSection(Props:any) {
  const bannerdata = Props.bannerdata;
  return (
    <>
      <section id="section1" className="w-full h-[calc(100vh-72px)] relative justify-center text-center flex items-center flex-col before:absolute before:content-[''] before:rotate-180 before:top-0 before:left-0 before:rotate-[-360deg] before:h-full before:w-[250px] xl:before:w-[280px] 2xl:before:w-[310px] 3xl:before:w-[345px] before:bg-cover before:bg-[url('../../public/images/left-side.png')] before:opacity-90 after:absolute after:content-[''] after:top-0 after:right-0 after:h-full after:w-[350px] xl:after:w-[400px] 2xl:after:w-[450px] 3xl:after:w-[560px] after:bg-cover after:bg-[url('../../public/images/right-side.png')] after:opacity-80 before:hidden lg:before:block after:hidden lg:after:block after:bg-center before:bg-center after:bg-cover before:bg-cover after:bg-no-repeat before:bg-no-repeat">
        <HomeBanner bannerdata={bannerdata}></HomeBanner>
        <Scrollto section="#process" label={bannerdata.attributes.headerSection.leftAnchorText} />
      </section>
    </>
  )
}
