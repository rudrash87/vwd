import Title from '@/components/ui/title';
import Imagetext from '@/components/sections/image-text';

export default function ProcessSection(Props:any) {

  const processdata = Props.data;
  const imagedata = processdata.attributes.processSection.titleContentImageLink;
  return (
    <>
      <section id="process">
        <div className='container'>
          <div className="max-w-[445px] mx-auto text-center pt-9 pb-9">
            <Title className='pb-5 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy' title={processdata.attributes.processSection.title} level={2} isHTML Iswhite></Title>
            {processdata.attributes.processSection.description && (
              <p>{processdata.attributes.processSection.description}</p>
            )}
          </div>
        </div>
        <div className='overflow-hidden'>
          <Imagetext tvarient className="text-image" textimage={imagedata}></Imagetext>
        </div>
      </section>
    </>
  )
}