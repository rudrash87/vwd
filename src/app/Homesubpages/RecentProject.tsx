"use client"
import Title from '@/components/ui/title';
import Button from '@/components/ui/button';
import { getMulticolorTitle, getMainTitle } from '@/components/ui/helper';
import SwiperItem from '@/components/sections/swiper-section';

export default function RecentProject(Props:any) {
  const data = Props.data;
  const projectarray = Props.recentdata;
  const multititle = getMulticolorTitle(data.attributes.projectSection.title);
  const title = getMainTitle(data.attributes.projectSection.title);
  return (
    <>
      <section id="section4" className='py-24 xl:py-36 2xl:py-48 3xl:py-56'>
        <div className='container'>
          <div className='w-full'>
            <Title className='text-2xl md:text-4xl tracking-[-1.5px] lg:text-5xl xl:text-6xl 3xl:text-7xl font-montheavy' title={title} Multicolor={multititle} level={2} isHTML Iswhite></Title>
          </div>
          <SwiperItem slidesView={1} navigation autoplay spacebetween={20} className="swiper-project" position={"default"} isHTML swiperdata={projectarray}></SwiperItem>
          <div className='w-full text-center pt-6'>
            <Button label={data.attributes.projectSection.link.text} href={data.attributes.projectSection.link.url} variant={"isdark"}></Button>
          </div>
        </div>
      </section>
    </>
  )
}
