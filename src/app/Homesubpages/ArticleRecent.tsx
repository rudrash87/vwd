"use client"
import Title from '@/components/ui/title';
import { getSwiperData } from "@/data/faker-data";
import Button from '@/components/ui/button';
import { getMulticolorTitle, getMainTitle } from '@/components/ui/helper';
import SwiperItem from '@/components/sections/swiper-section';

export default function ArticleRecent(Props:any) {

  const data = Props.data;
  const articlearray =Props.articledata;
  const multititle = getMulticolorTitle(data.attributes.articleSection.title);
  const title = getMainTitle(data.attributes.articleSection.title);
  return (
    <>

      <section id="section5" className='relative py-20 z-[1]'>
        <div className='container'>
          <div className='flex justify-between items-center pb-7'>
            <Title className="text-2xl md:text-4xl tracking-[-1.5px] lg:text-5xl xl:text-6xl 3xl:text-7xl font-montheavy" title={title} Multicolor={multititle} level={2} isHTML Iswhite></Title>
            <Button className="hidden md:block" label={"Voir tous les articles"} href="/" variant="isdark"></Button>
          </div>
        </div>
        <div className='w-full pl-5 pr-5 sm:pr-0 ml-auto mr-auto sm:mr-0 sm:max-w-[calc(50%+270px)] md:max-w-[calc(50%+360px)] lg:max-w-[calc(50%+480px)] xl:max-w-[calc(50%+540px)] 2xl:max-w-[calc(50%+697.5px)]'>
          <SwiperItem slidesView={3.1} navigation autoplay iscard spacebetween={20} className="swiper-article" position={"default"} isHTML swiperdata={articlearray}></SwiperItem>
        </div>
        <div className="z-[-1] absolute w-full md:w-[calc(100%-50px)] 2xl:w-[calc(100%-20px)] 3xxl:w-[calc(100%-50px)]  4xl:w-[calc(100%-165px)] h-full top-0 bottom-0 right-0 bg-backgroundDark border border-muted2 border-solid rounded-l-md"></div>
      </section>
    </>
  )
}