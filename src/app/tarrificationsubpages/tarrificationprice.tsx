"use client"
import Pricecard from "@/components/sections/pricecard";

export default function Tarrificationprice(Props:any) {
  const pricedata = Props.data;
  return (
    <>
        <section id="section2" className="pb-20">
            <div className="container">
                <div className="flex flex-wrap justify-center">
                    <Pricecard pricecard={pricedata.attributes.pricingSection.pricing} className="price-item"></Pricecard>
                </div>
            </div>
        </section>
    </>
  )
}