"use client"
import Title from "@/components/ui/title";
import FaqAccordion from "@/components/sections/faq-accordion";

export default function Tarrificationfaqs(Props:any) {
  const faqdata = Props.data;
  return (
    <>
        <section id="section5" className="">
            <div className="container">
                <Title title={faqdata.attributes.faqSection.title} level={3} Iswhite className="text-center pb-16"></Title>
                <div className=" ">
                    <div className="">
                        <Title title={faqdata.attributes.faqSection.faq1Title} level={4} Iswhite className="max-md:text-center font-montbold text-xl md:text-[28px] pb-9 px-2"></Title>
                        <div className="pb-20 2xl:pb-28">
                            <FaqAccordion className=" outline-none border-b border-info !border-t-0 border-solid text-lg md:text-xl py-3.5" faqs={faqdata.attributes.faqSection.faq1}></FaqAccordion>
                        </div>
                    </div>
                    <div className="">
                        <Title title={faqdata.attributes.faqSection.faq2Title} level={4} Iswhite className="max-md:text-center font-montbold text-xl md:text-[28px] pb-9 px-2"></Title>
                        <div className="pb-20 2xl:pb-28"><FaqAccordion className=" outline-none border-b border-info !border-t-0 border-solid text-lg md:text-xl py-3.5" faqs={faqdata.attributes.faqSection.faq2}></FaqAccordion></div>
                    </div>
                    <div className="">
                        <Title title={faqdata.attributes.faqSection.faq3Title} level={4} Iswhite className="max-md:text-center font-montbold text-xl md:text-[28px] pb-9 px-2 "></Title>
                        <div className="pb-20 2xl:pb-28"><FaqAccordion className=" outline-none border-b border-info !border-t-0 border-solid text-lg md:text-xl py-3.5" faqs={faqdata.attributes.faqSection.faq3}></FaqAccordion></div>
                    </div>
                </div>
            </div>
        </section>
    </>
  )
}
