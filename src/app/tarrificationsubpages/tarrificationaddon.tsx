"use client"
import Title from "@/components/ui/title";
import Feature from "@/components/sections/feature";

export default function Tarrificationaddon(Props:any) {
  const addondata = Props.data;
  return (
    <>
        <section id="section3" className="pb-20">
            <div className="container">
                <Title title={addondata.attributes.addonsSection.title} level={3} Iswhite isHTML className="text-center pb-5 md:pb-9"></Title>
                <Feature varient={"grid4"} className="" featuredata={addondata.attributes.addonsSection.addon}></Feature>
            </div>
        </section>
    </>
  )
}