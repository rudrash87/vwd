"use client"
import Title from "@/components/ui/title";

export default function Tarrificationbanner(Props:any) {
  const bannerdata = Props.data;
  return (
    <>
        <section id="section1" className="pt-16 md:pt-24">
            <div className="container">
                <div className="max-w-[912px] mx-auto text-center pb-6">
                    <Title title={bannerdata.attributes.title} className=" pb-8 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy " level={2} Iswhite></Title>
                    <p className="text-[17px] md:text-[19px] tracking-[0.26px] md:tracking-[0.28px] leading-[1.4] md:leading-[1.6]  font-montbook">{bannerdata.attributes.description}</p>
                </div>
            </div>
        </section>
    </>
  )
}
