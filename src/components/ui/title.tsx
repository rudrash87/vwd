import React, { type ReactNode } from "react";
import clsx from "clsx";

interface Props {
    title: string;
    Multicolor?: string;
    level?: 1 | 2 | 3 | 4 | 5 | 6;
    className?: string;
    isHTML?: boolean;
    Iswhite?: boolean;
}

const Title = ({
    title,
    Multicolor,
    level = 1,
    className,
    isHTML = true,
    Iswhite,
}: Props) => {
    const TagName = `h${level}`;

    const title_color = Iswhite &&"text-white";

    const levelColorClasses = {
        1: ``,
        2: ``,
        3: `font-montbold text-xl md:text-2xl lg:text-3xl 2xl:text-4xl`,
        4: `text-xl font-title`,
        5: ``,
        6: `text-base md:text-lg font-montbook leading-5 md:leading-9`,
    };

    const classes = clsx(levelColorClasses[level], className);

    return (
        <>
            {Multicolor ?
             <TagName className={classes}> {title} <span className="text-color-gradient">{Multicolor}</span></TagName>
             :
             <TagName className={classes}> {title} </TagName>
            }
        </>
    );
};

export default Title;