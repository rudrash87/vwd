import React, { type ReactNode } from "react";
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";

interface Props {
    children?: ReactNode;
    title?: string;
    Multicolor?: string;
    titlelevel?: 1 | 2 | 3 | 4 | 5 | 6;
    btndata?: any;
    className?: string;
    variant?: string;
    toptitle?: string;
}

function CTA({
    children, title, Multicolor, titlelevel, btndata, className, variant, toptitle,
}: Props): React.JSX.Element {
    function getVariant() {
        switch (variant) {
            case "default":
                return "full-w flex";
            case "half-w":
                return "items-center";
            case "70-w":
                return "70width";
            case "box-pack":
                return "border-box";
            default:
                return "full-w";
        }
    }
    const type_link = getVariant();
    return (
        <>
            {variant == "half-w" ? (
                <div className={`${type_link} ${className}`}>
                    {toptitle ? (
                        <div className="text-lg 2xl:text-xl text-danger2 font-bold pb-3.5">{toptitle}</div>
                    ) : ""
                    }
                    <Title title={title ? title : " "} Multicolor={Multicolor} className="pb-8 lg:pb-0 w-full lg:w-1/2 lg:pr-8 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" isHTML level={titlelevel}></Title>
                    <div className="w-full lg:w-1/2">
                        {children ?
                            <div className="">
                                <p className="text-base  2xl:text-lg 2xl:leading-8 tracking-[0.26px] 2xl:tracking-[0.28px]">{children}</p>
                            </div>
                            : ""
                        }
                        <div className="w-full -mx-2">
                            {btndata.map((btnitem: {
                                id: any;
                                label: any, url: any, bvariant: any, newwindow:boolean,
                            }, index: any) => (
                                
                                    <Button key={btnitem.id} newwindow={btnitem.newwindow} className="mt-5 mx-2" label={btnitem.label} href={btnitem.url} variant={btnitem.bvariant}></Button>
                                
                            ))}
                        </div>
                    </div>
                </div>
            )
            :
                <>
                {variant == "70-w" ? (
                    <div className={`${type_link} ${className}`}>
                        {toptitle ? (
                            <div className="text-lg 2xl:text-xl text-danger2 font-bold pb-3.5">{toptitle}</div>
                        ) : ""
                        }
                        <Title title={title ? title : " "} Multicolor={Multicolor} className="pb-8 trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" isHTML level={titlelevel}></Title>
                        <div className="w-full max-w-5xl">
                            {children ?
                                <div className="pb-6">
                                    <p className="text-base  2xl:text-lg 2xl:leading-8 tracking-[0.26px] 2xl:tracking-[0.28px]">{children}</p>
                                </div>
                                : ""
                            }
                            <div className="w-full -mx-2">
                                {btndata.map((btnitem: {
                                    id: any;
                                    label: any, url: any, bvariant: any, newwindow:boolean,
                                }, index: any) => (
                                    <>
                                        <Button key={btnitem.id} newwindow={btnitem.newwindow} className="mt-5 mx-2" label={btnitem.label} href={btnitem.url} variant={btnitem.bvariant}></Button>
                                    </>
                                ))}
                            </div>
                        </div>
                    </div>
                ) :
                    <div className={`section-${type_link} ${className}`}>
                        {toptitle ? (
                            <div className="text-lg 2xl:text-xl text-danger2 font-bold pb-3.5">{toptitle}</div>
                        ) : ""
                        }
                        <Title title={title ? title : " "} Multicolor={Multicolor} className="text-2xl md:text-4xl tracking-[-1.5px] lg:text-5xl xl:text-6xl 3xl:text-7xl font-montheavy text-center pb-6 lg:pb-8 xl:pb-12 2xl:pb-14 mx-auto max-w-[970px]" isHTML level={titlelevel}></Title>
                        {children ?
                            <div className="pb-6 lg:pb-8 xl:pb-12 2xl:pb-14 max-w-[545px] mx-auto">
                                <p className="text-base  md:text-lg md:leading-9 tracking-[0.26px] md:tracking-[0.28px]">{children}</p>
                            </div>
                            : ""
                        }
                        {btndata &&
                            <div className="btn-wrapper">
                                {btndata.map((btnitem: {
                                    id: any;
                                    label: any, url: any, bvariant: any,newwindow:boolean,
                                }, index: any) => (
                                    <>
                                        <Button key={btnitem.id} newwindow={btnitem.newwindow} className="m-3" label={btnitem.label} href={btnitem.url} variant={btnitem.bvariant}></Button>
                                    </>
                                ))}
                            </div>
                        }
                    </div>
                }
                </>
            }
        </>
    );
}

export default CTA;