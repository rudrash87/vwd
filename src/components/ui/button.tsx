import React, { type ReactNode } from "react";
import Link from 'next/link'

interface Props {
    className?: string;
    label?: string;
    href?: any;
    variant?: string;
    newwindow?: boolean;
}

function Button({
    newwindow, className, label, href, variant,
}: Props) {
    const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.preventDefault();
        const href = e.currentTarget.href;
        const targetId = href.replace(/.*\#/, "");
        const elem = document.getElementById(targetId);
        elem?.scrollIntoView({
            behavior: "smooth",
        });
    };
    const getVariant = () => {
        switch (variant) {
            case "isdark":
                return "bg-secondary tracking-[0.28px] text-base lg:text-lg font-montsemibold lg:font-montbold text-white py-4 px-6 rounded-md border-2 border-solid border-secondary inline-block hover:text-white hover:bg-transparent hover:border-white transition-32s";
            case "transparent":
                return "font-montsemibold lg:font-montbold text-base lg:text-lg tracking-[0.28px] text-danger py-4 px-6 rounded-md border-2 border-solid border-danger inline-block hover:text-muted2 hover:bg-danger transition-32s";
            case "without-border":
                return "plain-link text-base";
            default:
                return "transparent";
        }
    };
    const type_link = getVariant();
    return (
        <>
            {variant == 'without-border' ? 
                href.includes("#") ?
                    <Link target={`${newwindow ? "_blank" : ""}`} className={`link flex items-center ${className} ${type_link}`} href={href} onClick={handleScroll}><svg className="mr-2.5" version="1.0" xmlns="http://www.w3.org/2000/svg"
                        width="20px" height="20px" viewBox="0 0 512.000000 512.000000"
                        preserveAspectRatio="xMidYMid meet">

                        <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
                            fill="#000000" stroke="none">
                            <path className="fill-info" d="M2492 5109 c-45 -13 -108 -80 -121 -126 -7 -26 -11 -392 -11 -1130
                            l0 -1093 -1113 -2 -1113 -3 -41 -27 c-63 -41 -88 -90 -88 -169 0 -54 5 -72 27
                            -106 15 -22 44 -51 65 -64 l38 -24 1112 -3 1113 -2 2 -1113 3 -1112 24 -38
                            c13 -21 42 -50 64 -65 34 -23 52 -27 107 -27 55 0 73 4 107 27 22 15 51 44 64
                            65 l24 38 3 1112 2 1113 1113 2 1112 3 38 24 c21 13 50 42 65 64 23 34 27 52
                            27 107 0 55 -4 73 -27 107 -15 22 -44 51 -65 64 l-38 24 -1112 3 -1113 2 -2
                            1113 -3 1112 -24 38 c-47 76 -151 113 -239 86z"/>
                        </g>
                    </svg>{label}</Link>
                :
                    <Link target={`${newwindow ? "_blank" : ""}`} className={`link flex items-center ${className} ${type_link}`} href={href}><svg className="mr-2.5" version="1.0" xmlns="http://www.w3.org/2000/svg"
                        width="20px" height="20px" viewBox="0 0 512.000000 512.000000"
                        preserveAspectRatio="xMidYMid meet">

                        <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
                            fill="#000000" stroke="none">
                            <path className="fill-info" d="M2492 5109 c-45 -13 -108 -80 -121 -126 -7 -26 -11 -392 -11 -1130
                            l0 -1093 -1113 -2 -1113 -3 -41 -27 c-63 -41 -88 -90 -88 -169 0 -54 5 -72 27
                            -106 15 -22 44 -51 65 -64 l38 -24 1112 -3 1113 -2 2 -1113 3 -1112 24 -38
                            c13 -21 42 -50 64 -65 34 -23 52 -27 107 -27 55 0 73 4 107 27 22 15 51 44 64
                            65 l24 38 3 1112 2 1113 1113 2 1112 3 38 24 c21 13 50 42 65 64 23 34 27 52
                            27 107 0 55 -4 73 -27 107 -15 22 -44 51 -65 64 l-38 24 -1112 3 -1113 2 -2
                            1113 -3 1112 -24 38 c-47 76 -151 113 -239 86z"/>
                        </g>
                    </svg>{label}</Link>                
            : 
                href ?
                    href.includes("#") ?
                        <Link target={`${newwindow ? "_blank" : ""}`} className={`link ${className} ${type_link}`} href={href} onClick={handleScroll}>{label}</Link>
                    :
                        <Link target={`${newwindow ? "_blank" : ""}`} className={`link ${className} ${newwindow} ${type_link}`} href={href}>{label}</Link>
                :
                    ""
            }
        </>
    );
}

export default Button;