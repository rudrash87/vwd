"use client"
import React from "react";
import Link from "next/link";

export default function Scrollto(Props:any) {
    const secid = Props.section; 
    const handleScroll = (e: React.MouseEvent<HTMLAnchorElement, MouseEvent>) => {
        e.preventDefault();
        const href = e.currentTarget.href;
        const targetId = href.replace(/.*\#/, "");
        const elem = document.getElementById(targetId);
        elem?.scrollIntoView({
          behavior: "smooth",
        });
    };
    return (
        <>
            <div className="absolute bottom-36 md:bottom-16 bottom-csroll lg:bottom-28 -left-4 lg:left-3">
                <Link className="relative text-center hover:text-danger2 transition-32s" href={secid} onClick={handleScroll}><span className="-rotate-90 block">{Props.label}</span>
                <svg className="w-7 h-7 animate-bounce absolute left-[-15px] bottom-0 top-[65px]" version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 280 512" width="280" height="512">
                    <title>right-arrow-svg</title>
                    <defs>
                        <image  width="259" height="513" id="right-arrow-svg" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAQMAAAIBCAMAAAB6LypvAAAAAXNSR0IB2cksfwAAAY9QTFRF////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////7o0ZaQAAAIV0Uk5TABhQg7bp4698SRIDevn/8mUoyRr37jjt2wqzk2D+Qty8EPABQyd2XKaNsJgIIwykzeq+dzATkvsWYeQ5Z0j9+mt9aiGANs7EeFPCBQ2BKk9GimN7XoKFQUQk9gfHtzusAt0RWFSEUoZVh4iJVouM/I5Xj5CRlJWWmfNZWlv1IL3AaBe152F7lfoAAAc3SURBVHic7dpnkxVFGIbhAdYso6iIioi6JhQMiKwBRRED5oiCWYJZzIpZ4Ifrs0Xhhkk90+/b03Pu+yPdy3n6qv1AUacoKG1r1q6bO+/8Cy686OLUS1J1yaXry7NddnnqMWnacEW5pLkrU+9J0NqrymVtvDr1IvfWbCpXdM21qTd5d91KgrLcnHqTc9evJii33JB6lW9bKwzKG1Ovcu2mKoLy5vnUuzy7pdKgvDX1Ls9uqza4PfUuz+6oNtiWepdnd1Yb3JV6l2cYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYYCBwgADhQEGCgMMFAYYKAwwUBhgoDDAQGGAgcIAA4UBBgoDDBQGGCgMMFAYYKAwwEBhgIHCAAOFAQYKAwwUBhgoDDBQGGCgMMBAYTBmg+077nb6pNQG1S/dcM+995Xl+p3375p32JDSoO6lD+xeODdk54P2O9IZ1L70oY1Llzz8iPmSZAZ76l766GPLp+x93HpKKoMnVnzg3n1nD57ctGrMfuMtiQz2r/rELU8tHmx/umKNMUIag9UEZfnM4smzlXNsEZIYVBGU5QEdPVe9xxQhhUE1Qbn1+aJ4YaH6zBQhgcGLNc8sXyqKl+vOLBH8DWoJyleK4tXaQ0MEd4N6gvK1oni9/tQOwduggUCPfKPh2AzB2eDNpjceLIq3ms6tEHwNGgnKt4viUOMFIwRXg2aCw+/8d+XdBAieBs0E5Xu6837zHRMER4N1Lc/bs3jrA38EP4M2grkPF68d2uuO4GbQRvDRx2cvHqn757IZgpdBG8HhXeeuHvVGcDI41vKsheNLLnsj+Bi0Ehxddt0ZwcUgkMAbwcMgmMAZwcHgYDiBL4K9QS8CVwRzg54EngjWBr0JHBGMDT7pT+CHYGvQSvBp4487IZgaDCTwQrA0GEzghGBo8NlwAh8EO4MoBC4IZgaRCDwQrAyiETggGBlEJLBHsDH4PCaBOYKJQWQCawQLg+gExggGBgYEtgjxDb6wIDBFiG5gRGCJENvAjMAQIbKBIYEdQlyDhq8TDScwQ4hqYExghRDToJXgyz5/67JMECIaOBDYIMQz+MqDwAQhmoETgQVCLAM3AgOESAaOBPER4hh87UkQHSGKgTNBbIQYBu4EkREiGCQgiIsw3CAJQVSEwQbb0hDERBhqkIwgIsJAgzaCE3YE8RCGGSQliIYwyOCbtASxEIYYJCeIhDDAYAQEcRD6G7QSfDv8hR2KgNDb4LtxEMRA6GswGoIICD0NRkQwHKGfwagIBiP0Mvh+XARDEfoYjI5gIEIPgxESDEMINxglwSCEYIOREgxBCDX4YawEAxACDUZM0B8hzGDUBL0RggxGTtAXIcRg99gJeiIEGGRA0A+hu0EWBL0QOhu0EhzxeGGHwhG6GmzOhaAHQkeDjAjCEboZZEUQjNDJIDOCUIQuBtkRBCJ0MPgxP4IwhHaDLAmCEFoNMiUIQWgzyJYgAKHF4Kd8CbojNBtkTdAZoclgPnOCrggNBvkTdESoN5j/OX+Cbgi1BtMg6IRQZzAVgi4INQa/TIagA8LJoD/+n+DX1C8LqBWhV1kR2CBkRmCBkB1BfIQMCWIjZEkQFyFTgpgI2RLEQ8iYIBZC1gRxEDIniIGQPcFwhAkQDEWYBMEwhIkQDEGYDEF/hAkR9EU48Vvq3VHrgzAxgj4IkyMIR5ggQSjCJAnCECZKEIIwWYLuCBMm6IowaYJuCBMn6IIweYJ2hBkgaEOYCYJmhBkhaEKYGYJ6hBkiqEOYKYJqhBO/p17l3GqEmSMoiuMrvnz0x5T+77Brf/61lODvHan3JGn+n3O/Cif3zadek6pTB46dPnPm9LEDp1Ku+BcMjnapQLsQewAAAABJRU5ErkJggg=="/>
                    </defs>
                    <style>
                    </style>
                    <use id="right-arrow-svg" href="#right-arrow-svg" x="10" y="-1"/>
                </svg>
                </Link>
            </div>
        </>
    )
}