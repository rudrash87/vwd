const Pagination = ({items, pageSize, currentPage, onPageChange }) => {
 const pagesCount = Math.ceil(items / pageSize); // 100/10

 if (pagesCount === 1) return null;
 const pages = Array.from({ length: pagesCount }, (_, i) => i + 1);

 return (
  <>
    <div className="text-center">
     <ul className="paginations flex flex-wrap justify-center pt-10">
       {pages.map((page) => (
         <li
           key={page}
           className={
             page === currentPage ? "active mx-1.5 " : "noactive"
           }
         >
           <a className="inline-block bg-muted4 hover:bg-secondary transition-32s rounded py-1.5 px-3 text-sm font-montbold cursor-pointer w-[32px]  " onClick={() => onPageChange(page)}>
             {page}
           </a>
         </li>
       ))}
     </ul>
   </div>
  </>
 );
};

export default Pagination;