export function getMulticolorTitle(words: string){
    var n = words.trim().split(" ");
    return n[n.length - 1]
}

export function getMainTitle(words: string){
    var lastIndex = words.trim().lastIndexOf(" ");
    return words.substring(0, lastIndex);
}