import Image from 'next/image'
import Link from 'next/link'
import Title from './ui/title'
import Instagram from './icons/instagram'
import Linkedin from './icons/linkedin'
import Twitter from './icons/twitter'
import { AspectRatio } from '@radix-ui/react-aspect-ratio'
const Footer = (Props:any) => {
    const socialdataarray = Props.globaldata;
    const otherdataarray = Props.otherdata;
    const footerdata = Props.data;
    const menudata = footerdata.map((footeritem:any,index:any)=>(
        footeritem.attributes.MenuGroup
    ))
    const secondarydata = Props.secondarydata;
    const secondarymenudata = secondarydata.map((secmenuitem:any,index:any)=>(
        secmenuitem.attributes.link
    ))
  return (
    <section className=''>
        <section className=' shadow-[0_0px_20px_rgba(0,0,0,50)]'>
            <section className='container'>
                <section className='flex flex-wrap justify-center text-center pt-[70px] pb-11'>
                    <>
                        {menudata.map((menuitems:any,indexs:any)=>(
                            <>
                                {menuitems.map((menuitem:any,index:any)=>(
                                    <>
                                        {index == 0 ?
                                            <section key={index} className=' w-full sm:w-4/12 max-sm:py-9 md:w-3/12 lg:w-1/5'>
                                                <Title title={menuitem.title} level={4} className='font-montregular text-indigo text-xl mb-3.5' isHTML></Title>
                                                <ul className=''>
                                                    {menuitem.Item.map((menulist:any,index1:any)=>( 
                                                        <li key={index1} className=''>
                                                            <Link href={menulist.url} target={`${menulist.newWindow ? "_blank" : ""}`} className=' text-cyan text-sm hover:text-danger transition-32s'>{menulist.text}</Link>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </section>
                                        : ""
                                        }
                                    </>
                                ))}
                            </>
                        ))}
                    </>
                    <section className=' w-full sm:w-4/12 max-sm:py-9 md:w-3/12 lg:w-1/5'>
                        <Title title="Communauté" level={4} className='font-montregular text-indigo text-xl mb-3.5' isHTML></Title>
                        <div className=' flex flex-wrap justify-center'>
                            <>
                                {socialdataarray.attributes.SocialConfig.social.map((socailitem:any,index:any)=>(
                                    <>
                                        {index == 0 ?
                                            <div key={index} className=' px-2.5'>
                                                <Link target='_blank' href={socailitem.link} className=''>
                                                    <Instagram />
                                                </Link>
                                            </div>
                                        :
                                            index == 1 ?
                                                <div key={index} className=' px-2.5'>
                                                    <Link target='_blank' href={socailitem.link} className=''>
                                                        <Twitter />
                                                    </Link>
                                                </div>
                                            :
                                                <div key={index} className='social-item px-2.5'>
                                                    <Link target='_blank' href={socailitem.link} className=''>
                                                        <Linkedin />
                                                    </Link>
                                                </div>
                                        }
                                    </>
                                ))}
                            </>
                        </div>
                    </section>
                    <>
                        {menudata.map((menuitems:any,indexs:any)=>(
                            <>
                                {menuitems.map((menuitem:any,index:any)=>(
                                    <>
                                        {index == 1 ?
                                            <section key={index} className=' w-full sm:w-4/12 max-sm:py-9 md:w-3/12 lg:w-1/5'>
                                                <Title title={menuitem.title} level={4} className='font-montregular text-indigo text-xl mb-3.5' isHTML></Title>
                                                <ul className=''>
                                                    {menuitem.Item.map((menulist:any,indexl:any)=>( 
                                                        <li key={indexl} className=''>
                                                            <Link href={menulist.url} target={`${menulist.newWindow ? "_blank" : ""}`} className=' text-cyan text-sm hover:text-danger transition-32s'>{menulist.text}</Link>
                                                        </li>
                                                    ))}
                                                </ul>
                                            </section>
                                        : ""
                                        }
                                    </>
                                ))}
                            </>
                        ))}
                    </>
                </section>
            </section>
        </section>
        
        <section className=' pt-12 pb-3.5'>
            <div className="container">
                <div className=' text-center'>
                    <div className='text pb-4'>
                        <p className='text-sm 2xl:text-base text-info' >{otherdataarray.attributes.otherWebsites.text}</p>
                    </div>
                    <div className=' flex flex-wrap justify-center'>
                        <>
                            {otherdataarray.attributes.otherWebsites.website.map((websiteitem:any,index:any)=>(
                                <>
                                    <div key={index} className='py-4 px-3.5 md:py-0 w-[189px] f-img transition-32s scale-100 hover:scale-105'>
                                        <Link target='_blank' href={websiteitem.link} className='w-full block'>
                                            {websiteitem.logo.data &&
                                                <Image src={websiteitem.logo.data.attributes.url} alt={websiteitem.logo.data.attributes.alternativeText} fill className='object-contain'></Image>
                                            }
                                        </Link>
                                    </div>
                                </>
                            ))}
                        </>
                    </div>
                </div>
            </div>
        </section>
        <section className=''>
            <div className=' pl-10 max-sm:pr-10  pl-8 pb-16 sm:pb-3.5'>
                <ul className=' flex flex-wrap flex-col sm:flex-row justify-center xl:justify-start text-center'>
                    <>
                        {secondarymenudata.map((menuitems:any,indexs:any)=>(
                            <>
                                {menuitems.map((menuitem:any,index:any)=>(
                                    <>
                                        <li key={index} className=' px-3.5'>
                                            <Link href={`/content${menuitem.url}`} target={`${menuitem.newWindow ? "_blank" : ""}`} className='font-montlight text-sm text-info hover:text-danger transition-32s'>{menuitem.text}</Link>
                                        </li>
                                    </>
                                ))}
                            </>
                        ))}
                    </>
                </ul>
            </div>
        </section>
    </section>
  )
}

export default Footer