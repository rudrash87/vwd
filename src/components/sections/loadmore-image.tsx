import React, { type ReactNode } from "react";
import Image from 'next/image';

interface Props {
    className?: string;
    initialimte?: any;
    loadlimit?: any;
    imagelinkdata?: any;
}
function Loadmoreimage({
    className, imagelinkdata, initialimte, loadlimit
}: Props) {
    const [displayPosts, setDisplayPosts] = React.useState(initialimte);
    const [articles, setArticles] = React.useState(imagelinkdata);

    const loadMore = () => {
        setDisplayPosts(displayPosts + loadlimit)
    }
    return (
        <>
            <div className={`section-loadmore ${className}`}>
                {imagelinkdata.slice(0, displayPosts).map((textimageitem:{
                    url: any;},index: React.Key | null | undefined) => (
                    <div className="text-image-inner" key={index}>
                        <Image src={textimageitem.url} height={200} width={150} alt="{textimageitem.title}"></Image>
                    </div>  
                ))}
                {displayPosts < articles.length ? ( 
                    <button onClick={loadMore}>Load More</button>
                ) : null}
            </div>
        </>
    );
}

export default Loadmoreimage;