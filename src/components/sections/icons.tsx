import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import { AspectRatio } from "@/components/ui/aspect-ratio";

interface Props {
    className?: string;
    textimage?: any;
    tvarient?: any;
}

function Icons({
    className, textimage, tvarient
}: Props) {
    return (
        <>
            <div className="flex flex-wrap -mx-4 xl:-mx-5 2xl:-mx-6 pt-4 xl:pt-5 2xl:pt-6">
                {tvarient == "6gird" ?
                    textimage.map((textimageitem: {
                        image: any;
                        },index: React.Key | null | undefined) => (
                        <div className="w-1/3 pb-5 lg:pb-0 lg:w-1/6 px-4 xl:px-5 2xl:px-6" key={index}>
                            <Image className="w-[120px]" src={textimageitem.image} fill={true} alt={"image"}></Image>
                        </div>
                    ))
                :
                    textimage.map((textimageitem: {
                        image: any;
                        },index: React.Key | null | undefined) => (
                        <div className="w-1/3 px-4 xl:px-5 2xl:px-6" key={index}>
                            <Image className="w-[120px]" src={textimageitem.image} fill={true} alt={"image"}></Image>
                        </div>
                    ))
                }
            </div>
        </>
    );
}

export default Icons;