import React, { type ReactNode } from "react";
import Image from 'next/image'
import dayjs from "dayjs";
import Title from "@/components/ui/title";
import { AspectRatio } from "@/components/ui/aspect-ratio";
import Twitter from "../icons/twitter";
import Instagram from "../icons/instagram";
import Linkedin from "../icons/linkedin";
import Link from "next/link";
import {
    TwitterShareButton, LinkedinShareButton,
    FacebookIcon,
  } from 'next-share'
interface Props {
    className?: string;
    articledata?: any;
}

function ArticleBanner({
    className, articledata, 
}: Props) {
    return (
        <>
            <div className={`flex flex-wrap sm:pt-12 pb-12 sm:px-9 md:px-10 lg:px-14 xl:px-16 2xl:px-20 border border-muted2 rounded-md bg-whiteDark  ${className}`}>
                {articledata.map((articleitem:any,index:any) => (
                    <>
                        <div className=" w-full md:w-3/5 lg:pr-16 xl:pr-20 2xl:pr-28 max-sm:px-9">
                            <div className="pb-3.5 w-fit pr-3">
                                <div className=" text-sm 2xl:text-base bg-info5 py-1.5 px-4 font-semibold tracking-[0.24px]">{articleitem.cat}</div>
                            </div>
                            <div className=" pb-3.5">
                                <Title title={articleitem.title} level={2} className="trackig-[-0.66px] lg:tracking-[-1.04px] 2xl:leading-[90px] text-3xl md:text-4xl lg:text-5xl 2xl:text-[52px] font-montheavy" Iswhite isHTML></Title>
                            </div>
                            <div className=" pb-8">
                                <p className="text-sm 2xl:text-base text-info font-semibold tracking-[0.24px]" >{dayjs(articleitem.date).format("DD/MM/YYYY")}</p>
                            </div>
                            
                            <div className="flex md:hidden flex-wrap justify-start pb-8" >
                                <span className="text-success text-sm font-montbok">{'Partager sur :'}</span>
                                <div className=' px-2.5'>
                                    <TwitterShareButton url={`${articleitem.slug}`}>
                                        <Twitter />
                                    </TwitterShareButton>
                                </div>
                                <div className=' px-2.5'>
                                    <Link href='/' target="_blank" >
                                        <Instagram />
                                    </Link>
                                </div>
                                <div className='social-item px-2.5'>
                                    <LinkedinShareButton url={`${articleitem.slug}`}>
                                        <Linkedin />
                                    </LinkedinShareButton>
                                </div>
                            </div>

                            <div className=" flex flex-wrap items-center">
                                <div className="rounded-full overflow-hidden w-16 h-16 mr-2.5"><Image className="w-full h-full" src={articleitem.auth_image} alt={articleitem.auth_image_alt} width={100} height={100}></Image></div>
                                <div >
                                    <div className="text-sm 2xl:text-base text-success">{"Rédigé par"}</div>
                                    <div className="text-sm 2xl:text-base text-success">{articleitem.written_by}</div>
                                </div>
                            </div>
                            
                        </div>
                        <div className=" w-full md:w-2/5 max-md:-order-1 ">
                            <div className="rounded-md overflow-hidden  mb-5 md:mb-8">
                                <AspectRatio ratio={12 / 7}>
                                    <Image className="w-full  object-top object-cover  transition-32s scale-100 hover:scale-105 " src={articleitem.main_image} alt={articleitem.main_image_alt} fill></Image>
                                </AspectRatio>
                            </div>
                            <div className=" hidden md:flex flex-wrap justify-end" >
                                <span className="text-success text-sm font-montbok">{'Partager sur :'}</span>
                                <div className=' px-2.5'>
                                    <TwitterShareButton url={`${articleitem.slug}`}>
                                        <Twitter />
                                    </TwitterShareButton>
                                </div>
                                <div className=' px-2.5'>
                                    <Link href='/' target="_blank">
                                        <Instagram />
                                    </Link>
                                </div>
                                <div className='social-item px-2.5'>
                                    <LinkedinShareButton url={`${articleitem.slug}`}>
                                        <Linkedin />
                                    </LinkedinShareButton>
                                </div>
                            </div>
                        </div>
                    </> 
                ))}
            </div>
        </>
    );
}

export default ArticleBanner;