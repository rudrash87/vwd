import React, { type ReactNode } from "react";
import Title from "@/components/ui/title";

interface Props {
    className?: string;
    techdescriptiondata?: any;
}

function Techdescription({
    className, techdescriptiondata, 
}: Props) {
    return (
        <>
            {techdescriptiondata.map((techdescriptionitem: {
                id: any;
                title: any;
                name: any;
            },index: React.Key | null | undefined) => (
                    <div className={`flex justify-between border-inherit border-b border-solid py-4 ${className}`} key={techdescriptionitem.id}>
                        <Title title={techdescriptionitem.title} Iswhite className="text-base md:text-lg font-montbook text-success leading-5 md:leading-9" level={6} isHTML></Title>
                        <p className="text-base md:text-lg font-montbold leading-5 md:leading-9 text-successDark">{techdescriptionitem.name}</p>
                    </div> 
                ))
            }
        </>
    );
}

export default Techdescription;