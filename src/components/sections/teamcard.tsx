import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";
import { AspectRatio } from "@/components/ui/aspect-ratio";

interface Props {
    className?: string;
    teamcarddata?: any;
}

function Teamcard({
    className, teamcarddata,
}: Props) {
    return (
        <>
            <div className="flex flex-wrap">
                {teamcarddata.map((teamcarditem: {
                    id: any;
                    name: any;
                    designation: string | undefined;
                    image: any;
                    role: any;
                }, index: React.Key | null | undefined) => (
                    <div className={` ${className}`} key={teamcarditem.id}>
                        <div className="w-full overflow-hidden">
                            <div className="mb-5 overflow-hidden">
                                {teamcarditem.image.data?
                                    <AspectRatio ratio={104 / 160}>
                                        <Image className="overflow-hidden scale-100 hover:scale-105 transition-32s" src={teamcarditem.image.data.attributes.url} fill={true} alt={teamcarditem.image.data.attributes.alternativeText} />
                                    </AspectRatio>
                                :
                                    <AspectRatio ratio={104 / 160}>
                                        <Image className="overflow-hidden scale-100 hover:scale-105 transition-32s" src={teamcarditem.image} fill={true} alt={teamcarditem.name} />
                                    </AspectRatio>
                                }
                            </div>
                            <Title title={teamcarditem.name} Iswhite className="leading-9 tracking-normal text-xl 2xl:text-2xl" level={5} isHTML></Title>
                            <div className="designation">
                                {teamcarditem.role ?
                                    <p className="text-success tracking-normal text-base  2xl:text-lg">{teamcarditem.role}</p>
                                :
                                    <p className="text-success tracking-normal text-base  2xl:text-lg">{teamcarditem.designation}</p>
                                }
                            </div>
                        </div>
                    </div>
                ))
                }
            </div>
        </>
    );
}

export default Teamcard;