import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import { AspectRatio } from "@/components/ui/aspect-ratio";
import Twitter from "../icons/twitter";
import Instagram from "../icons/instagram";
import Linkedin from "../icons/linkedin";
import Link from "next/link";

interface Props {
    title?: any;
    className?: string;
    articledata?: any;
}

function RecentArticle({
    className, articledata, title,
}: Props) {
    console.log(articledata)
    return (
        <>
            <div className={` ${className}`}>
                {articledata.map((articleitem:any,index:any) => (
                    <>
                        <div className=" max-lg:w-1/2 max-md:w-full max-md:px-2.5" key={index}>
                            <div className=" mb-6 lg:mb-12 p-5 lg:p-9 border border-muted2 rounded-md overflow-hidden bg-muted cursor-pointer">
                                <div className="pb-3.5">
                                    <Title className="text-xl 2xl:text-2xl font-montbold" title={title} isHTML Iswhite level={5}></Title>
                                </div>
                                <div className="mb-4 rounded-md overflow-hidden">
                                <AspectRatio ratio={299 / 190}>
                                    {articleitem.image ?
                                        <Image className="w-full object-top object-cover  transition-32s scale-100 hover:scale-105" src={articleitem.image} fill alt={articleitem.title}></Image>
                                    :
                                        <Image className="w-full object-top object-cover  transition-32s scale-100 hover:scale-105" src={articleitem.attributes.image.data.attributes.url} fill alt={articleitem.attributes.image.data.attributes.alternativeText}></Image>
                                    }
                                </AspectRatio>
                                </div>
                                <div className="pb-2.5 pt-3.">
                                    {articleitem.title ?
                                        <Title level={5} title={articleitem.title} className=" text-success font-montsemibold text-lg text-[22px] leading-[1.2] tracking-[0.4px]" isHTML Iswhite></Title>
                                    :
                                        <Title level={5} title={articleitem.attributes.title} className=" text-success font-montsemibold text-lg text-[22px] leading-[1.2] tracking-[0.4px]" isHTML Iswhite></Title>
                                    }
                                </div>
                                {articleitem.attributes?.content ?
                                    articleitem.attributes.content.map((contentitem:any,indexitem:any)=>(
                                        indexitem == 0 && 
                                            contentitem.content ?
                                                <p className=" text-sm 2xl:text-base text-success font-montbook tracking-[0.26px] pb-4" key={indexitem}>
                                                    {contentitem.content.split(' ').slice(0, 10).join(' ')}
                                                </p>
                                            :
                                                ""
                                    ))
                                :
                                    ""
                                }
                                {articleitem.attributes ?
                                    <Button label="Lire la suite" href={` /blog/${articleitem.attributes.slug}`} className="" variant="isdark"></Button>
                                :
                                    <Button label="Lire la suite" href={articleitem.image} className="" variant="isdark"></Button>
                                }
                            </div>
                        </div>
                    </> 
                ))}
            </div>
        </>
    );
}

export default RecentArticle;