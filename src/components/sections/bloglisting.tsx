import React, { type ReactNode } from "react";
import Image from "next/image";
import dayjs from "dayjs";
import Title from "../ui/title";
import { getBlogs } from "@/data/faker-data";
import { AspectRatio } from "@radix-ui/react-aspect-ratio";
import RecentArticle from "./recent-article";
import { BlogRecentData } from "@/data/blogpage-data";
import { useState } from "react";
import Pagination from "../ui/Pagination";
import { paginate } from "../ui/paginate";

type Props = {
  selectedGenre?: string;
  catdata?: any;
};
function filter_data(catdata:any,selectedGenre:any){
  let filterarray = new Array();
  catdata.map((movie:any)=>
    movie.attributes.relatedCategories.data &&
      movie.attributes.relatedCategories.data.map((cat:any)=>
        cat.attributes.name == selectedGenre &&
          filterarray.push(movie)
      )
  )
  return filterarray
}
function Bloglisting({ selectedGenre, catdata }: Props) {
  // const filter_data = [];
  const getblogdata = getBlogs(1);
  const [currentPage, setCurrentPage] = useState(1);
  const pageSize = 5;
  const onPageChange = (page:any) => {
      setCurrentPage(page);
  };
  const filteredMoviesByGenre = selectedGenre
    ? 
      filter_data(catdata,selectedGenre)
    : 
      catdata;
  const paginatedPosts = paginate(filteredMoviesByGenre, currentPage, pageSize);
  return (
    <>
      <div className=" flex flex-wrap wrap pb-16 xl:pb-20 max-md:px-2.5 ">
        {/* map through all the filtered movies */}
        {paginatedPosts.map((movie: {
          image: string; id: React.Key | null | undefined; title: any; category: any; attributes:any;
        }, index: any) => {
          return (
            index == 0 ? (
              <div className="w-full" key={index}>
                <a href={`/blog/${movie.attributes.slug}`} key={movie.id} className="block full-w shadow-sm cursor-pointer">
                  <div className="flex flex-wrap  bg-whiteDark  max-md:border max-md:rounded-md border-muted2">
                    <div className="w-full md:w-3/5  rounded-md overflow-hidden max-md:p-4">
                      <AspectRatio ratio={199 / 93}>
                        <Image className="w-full object-top object-cover  transition-32s scale-100 hover:scale-105" src={movie.attributes.image.data.attributes.url} alt={movie.attributes.image.data.attributes.alternativeText} fill></Image>
                      </AspectRatio>
                    </div>

                    <div className="w-full md:w-2/5 max-md:py-8 pl-5 sm:pl-7 lg:pl-12 pr-5  md:border-t md:border-b md:border-r md:border-muted2 md:rounded-r-md  flex flex-col justify-center">
                      {movie.attributes.relatedCategories.data ?
                        movie.attributes.relatedCategories.data.map((cat:any,index2:any)=>(
                          <div className="pb-4 w-fit pr-3 " key={index2}><p className="text-sm 2xl:text-base bg-info5 py-1.5 px-4 tracking-[0.24px] font-montsemibold">Genres: {cat.attributes.name}</p></div>
                        ))
                      :
                       ""
                      }
                      <div className="pb-[18px]"><Title level={5} title={movie.attributes.title} className="text-lg md:text-[22px] md:leading-[1.27] tracking-[0.32px] md:tracking-[0.4px] font-semibold text-success"></Title></div>
                      <div className="text-sm 2xl:text-base text-info font-montsemibold tracking-[0.21px] sm:tracking-[0.24px]">{dayjs(movie.attributes.publicationDate).format("DD/MM/YYYY")}</div>
                    </div>

                  </div>
                </a>
              </div>
            )
              : ""

          );
        })}
      </div>
      <div className=" flex flex-wrap ">
        <div className="w-full lg:w-[70%]">
          {/* map through all the filtered movies */}
          {paginatedPosts.map((movie: {
            image: string; id: React.Key | null | undefined; title: any; category: any; attributes:any;
          }, index: any) => {
            return (
              index == 0 ? (
                "")
                :

                <div key={index} className="w-full pb-7 md:pb-6 max-md:px-2.5">
                  <a href={`/blog/${movie.attributes.slug}`} className="block half-w shadow-sm rounded-md cursor-pointer">
                    <div className="flex flex-wrap  bg-whiteDark items-center ">
                      <div className="w-full md:w-1/2 rounded-md overflow-hidden ">
                      <AspectRatio ratio={224 / 135}>
                        <Image className="w-full object-top object-cover  transition-32s scale-100 hover:scale-105" src={movie.attributes.image.data.attributes.url} alt={movie.attributes.image.data.attributes.alternativeText} fill></Image>
                      </AspectRatio>
                      </div>
                      <div className="w-full md:w-1/2 max-md:py-8 md:pl-6 lg:pl-8 md:pr-5">
                        {movie.attributes.relatedCategories.data ?
                          movie.attributes.relatedCategories.data.map((cat:any,index2:any)=>(
                            <div className="pb-4 w-fit pr-3" key={index2}><p className="text-sm 2xl:text-base bg-info5 py-1.5 px-4 tracking-[0.24px]">Genres: {cat.attributes.name}</p></div>
                          ))
                        :
                          ""
                        }
                        <div className="pb-5"><Title title={movie.attributes.title} level={5} className="text-lg 2xl:text-[22px] 2xl:leading-[1.2] font-semibold text-success tracking-[0.32px] xl:tracking-[0.4px]"></Title></div>
                        <div className="text-sm 2xl:text-base text-info font-montsemibold tracking-[0.21px] sm:tracking-[0.24px]">{dayjs(movie.attributes.publicationDate).format("DD/MM/YYYY")}</div>
                      </div>
                    </div>
                  </a>
                </div>

            );
          })}
        </div>
        <div className="w-full lg:w-[30%]">
          <div className="" >
            <BlogRecentData />
          </div>
        </div>
      </div>
      <Pagination items={filteredMoviesByGenre.length}  currentPage={currentPage}  pageSize={5}  onPageChange={onPageChange}/>
    </>
  );
};
export default Bloglisting;