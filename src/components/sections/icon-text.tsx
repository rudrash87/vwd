import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";

interface Props {
    className?: string;
    textimage?: any;
}

function Icontext({
    className, textimage, 
}: Props) {
    return (
        <>
            {textimage.map((textimageitem: {
                    link: string;
                    image: any;
                    logo: any;
                    name: any;
                    title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                    <div className={` w-full md:w-1/2 lg:w-1/3 sm:px-3 py-2 ${className}`} key={index}>
                        <div className="border border-muted2 px-6 xl:px-10 py-8 h-full rounded-md bg-black shadow-[0_0px_50px_rgb(71,32,80,0.2)]">
                            <div className=" flex justify-end pb-9">
                                <div className="w-auto">
                                    {textimageitem.logo.data ?
                                        <Image className="w-full object-contain" src={textimageitem.logo.data.attributes.url} height={200} width={150} alt={textimageitem.logo.data.attributes.alternativeText}></Image>
                                    :
                                        <Image className="w-full object-contain" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                    }
                                </div>
                            </div>
                            <div>
                                {textimageitem.name ?
                                    <Title title={textimageitem.name} className="uppercase font-montbold text-danger2 leading-7 pb-2.5 text-xl 2xl:text-2xl tracking-[0.3px] 2xl:tracking-[0.38px] " level={5} isHTML></Title>
                                :
                                    <Title title={textimageitem.title} className="uppercase font-montbold text-danger2 leading-7 pb-2.5 text-xl 2xl:text-2xl tracking-[0.3px] 2xl:tracking-[0.38px] " level={5} isHTML></Title>
                                }
                                <div>
                                    <p className="font-montbook text-success text-base tracking-[0.26px]">{textimageitem.description}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                ))
            }
        </>
    );
}

export default Icontext;