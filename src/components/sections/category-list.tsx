import React, { type ReactNode } from "react";

type Props = {
    selectedGenre: string;
    onSelect(genre: string): void;
    catdata?: any;
  };

  function CategoryList({ selectedGenre, onSelect, catdata }: Props) {
    const splitGenres = catdata.map((movie: { attributes: any; }) => movie.attributes.name);
    // const final_Cat = splitGenres.map((item: {name: string;}) => item.name);
    const catarray = Array.from(new Set(splitGenres));
    return (
        <ul className="flex flex-wrap sm:justify-center bg-info4 px-5 sm:px-7 py-3 rounded-md cat-list  ">
          <li
            className={!selectedGenre ? "active transition-32s p-1 " : "  transition-32s p-1"}
            onClick={() => onSelect("")}
          >
            <a className="text-success hover:bg-secondary rounded-md hover:text-white text-sm 2xl:text-base font-montbook  cursor-pointer py-1.5 md:py-2 px-3.5 block rounded-md transition-32s">
              All
            </a>
          </li>
          {catarray.map((genre: any, i: any) => {
            const isSelected = genre === selectedGenre;

            return (
                <li
                  key={i}
                  // use a different class if the genre is selected.
                  className={isSelected ? "active transition-32s p-1" : " transition-32s p-1"}
                  // attach the onSelect handler
                  onClick={() => onSelect(genre)}
                >
                  <a className="text-success hover:bg-secondary rounded-md hover:text-white text-sm 2xl:text-base font-montbook  cursor-pointer py-1.5 md:py-2 px-3.5 block rounded-md transition-32s">
                    {genre}
                  </a>
                </li>
              );
          })}
        </ul>
      );
  };

export default CategoryList;