import React, { type ReactNode } from "react";
import Title from "@/components/ui/title";

interface Props {
    className?: string;
    reviewdata?: any;
}

function Customerreivew({
    className, reviewdata, 
}: Props) {
    return (
        <>
            <div className="border-l-8 border-solid border-secondary pl-6">
                {reviewdata.map((reviewdataitem: {
                    text: string;
                    name: any;
                    designation: any;
                    id:any; },index: React.Key | null | undefined) => (
                    <div className="" key={index}>
                        <Title title={"Lorem ipsum dolor"} className="text-successDark font-montbold pb-5 md:pb-2" level={4} Iswhite isHTML></Title>
                        <div className="text-sm font-montbookitalic text-success leading-6">
                            {reviewdataitem.text}
                        </div>
                        <div className="pt-5 md:pt-1">
                            <p className="font-montsemibold leading-6 lg:leading-10 text-base tracking-normal md:text-lg text-success"> {reviewdataitem.name} / {reviewdataitem.designation}</p>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}

export default Customerreivew;