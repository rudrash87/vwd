import React, { type ReactNode } from "react";
import Title from "@/components/ui/title";
import Image from "next/image";

interface Props {
    basicdata?: any;
}

function BasicText({
    basicdata, 
}: Props) {
    return (
        <>
            {basicdata.map((basicitem : {
                id: any;
                title: any;
                text: any;
                content: any;
                media: any,
                },index: React.Key | null | undefined) => ( 
                    <div className=" pb-8 md:pb-9" key={index}>
                        {basicitem.title && (
                            <div className="pb-5 md:pb-6"><Title title={basicitem.title} className="text-2xl font-montbold lg:text-3xl xl:text-4xl 2xl:text-[42px] " level={4} Iswhite></Title></div>
                        )}
                        {basicitem.text ? (
                            <div className="text-success text-sm 2xl:text-base  leading-7 font-montbook tracking-[0.26px]" dangerouslySetInnerHTML={{ __html: basicitem.text }}></div>
                        )
                        :
                            <div className="text-success text-sm 2xl:text-base  leading-7 font-montbook tracking-[0.26px]" dangerouslySetInnerHTML={{ __html: basicitem.content }}></div>
                        }
                        {basicitem.media?.data && (
                            <Image src={basicitem.media.data?.attributes.url} alt={basicitem.media.data?.attributes.alternativeText} fill></Image>
                        )}
                    </div>
                ))
            }
        </>
    );
}

export default BasicText;