"use client"
import Image from "next/image";
import dayjs from "dayjs";
import React, { type ReactNode, useRef, useState } from "react";
import { Swiper, SwiperSlide } from 'swiper/react'
import 'swiper/css'
import 'swiper/css/navigation'
import 'swiper/css/pagination'
import "swiper/css/grid";
import { AspectRatio } from "@radix-ui/react-aspect-ratio";
import Link from "next/link";
import Button from "../ui/button";
import Title from '@/components/ui/title';
import { Grid, Navigation, Pagination, Autoplay } from 'swiper/modules'

interface Props {
    swiperdata: any;
    slidesView?: number;
    className?: string;
    isHTML?: boolean;
    position?: string;
    navigation?: boolean;
    autoplay?: boolean;
    iscard?: boolean;
    isclient?: boolean;
    spacebetween?: number;
}
const SwiperItem = ({
    swiperdata,
    slidesView,
    position,
    className,
    isHTML = true,
    navigation,
    autoplay,
    iscard,
    isclient,
    spacebetween,
}: Props) => {
    const getLayoutPosition = () => {
        switch (position) {
            case "reverse":
                return "flex-row-reverse";
            case "centered":
                return "flex-col-reverse justify-center max-w-4xl card-box-alignment text-left";
            default:
                return "md:flex-row";
        }
    };

    const parentClass = getLayoutPosition();
    const cardclass = iscard ? "card-items" : "full-width";
    return (
        <>
            <div className={`media-swiper ${cardclass} ${parentClass}`}>
                {iscard ?

                    <Swiper
                        slidesPerView={slidesView}
                        spaceBetween={spacebetween}
                        pagination={{
                            el: ".swiper-pagination",
                            clickable: true,
                            type: 'fraction',
                        }}
                        navigation={{
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }}
                        breakpoints={{
                            "992": {
                                slidesPerView: 3.5,
                            },
                            "768": {
                                slidesPerView: 2.2,
                                grid: {
                                    rows: 2,
                                    fill: 'row',
                                },
                            },
                            "240": {
                                slidesPerView: 1,
                                grid: {
                                    rows: 2,
                                    fill: 'row',
                                },
                            },
                        }}
                        autoplay={autoplay}
                        className={`media-swiper w-ful ${className}`}
                        modules={[Grid, Navigation, Pagination, Autoplay]}
                    >
                        {swiperdata?.map((swiperitem: any, index: React.Key | null | undefined) => (
                            <SwiperSlide key={index}>

                                <div className="w-full relative">
                                    <div className="w-full relative">
                                        {swiperitem.attributes ?
                                            swiperitem.attributes.image.data &&
                                                <Link href={swiperitem.attributes.slug}>
                                                    <Image className="w-full h-[auto]" src={swiperitem.attributes.image.data.attributes.url} alt={swiperitem.attributes.image.data.attributes.alternativeText} width={200} height={200}></Image>
                                                </Link>
                                        :
                                            <Image className="w-full h-[auto]" src={swiperitem.attributes.image.data.attributes.url} alt={swiperitem.attributes.image.data.attributes.alternativeText} width={200} height={200}></Image>
                                        }
                                        {swiperitem.attributes ?
                                            <div className="absolute bg-secondary bottom-6 left-0 text-sm 2xl:text-base py-1.5 px-3.5 font-montheavy">{dayjs(swiperitem.attributes.publicationDate).format("DD/MM/YYYY")}</div>
                                        :
                                            <div className="absolute bg-secondary bottom-6 left-0 text-sm 2xl:text-base py-1.5 px-3.5 font-montheavy">{swiperitem.date}</div>
                                        }
                                    </div>
                                    <div className="pt-5">
                                        <div className="text-sm 2xl:text-base leading-8 md: md:tracking-[0.29px] md:tracking-[0.36px] montsemibold">
                                            {swiperitem.attributes ?
                                                <Link className="before:absolute before:content-[''] before:top-0 before:left-0 before:right-0 before:buttom-0 before:h-full before:w-full z-10" href={`blog/ ${swiperitem.attributes.slug}`}>
                                                    <p>{swiperitem.attributes.title}</p>
                                                </Link>
                                            :
                                                <p>{swiperitem.description}</p>
                                            }
                                        </div>
                                    </div>
                                </div>

                            </SwiperSlide>
                        ))}
                        <div className="flex items-center bg-muted5 py-3.5 px-7 rounded-md w-fit mx-auto lg:m-0 shadow-[0_3px_20px_rgb(0,0,0,16)]">
                            <div className="swiper-pagination mr-9"></div>
                            <div className="swiper-btn-wrapper flex items-center">
                                <div className="swiper-button-prev after:hidden rotate-180 opacity-100 mr-2.5 h-fit">
                                    <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                        <title>right-arrow-svg</title>
                                        <defs>
                                            <image  width="512" height="258" id="img1111" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                        </defs>
                                        <style>
                                        </style>
                                        <use id="img1" href="#img1111" x="0" y="11"/>
                                    </svg>
                                </div>
                                <div className="swiper-button-next after:hidden opacity-100 h-fit">
                                    <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                        <title>right-arrow-svg</title>
                                        <defs>
                                            <image  width="512" height="258" id="img1021" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                        </defs>
                                        <style>
                                        </style>
                                        <use id="img1" href="#img1021" x="0" y="11"/>
                                    </svg>
                                </div>
                            </div>
                        </div>
                    </Swiper>
                    :

                    <Swiper
                        slidesPerView={slidesView}
                        spaceBetween={spacebetween}
                        pagination={{
                            el: ".swiper-pagination",
                            clickable: true,
                            type: 'fraction',
                        }}
                        navigation={{
                            nextEl: '.swiper-button-next',
                            prevEl: '.swiper-button-prev',
                        }}
                        autoplay={autoplay}
                        className={`media-swiper md:pr-[16%] w-ful ${className}`}
                        modules={[Grid, Navigation, Pagination, Autoplay]}
                    >
                        {swiperdata?.map((swiperitem: any, index: React.Key | null | undefined) => (
                            <SwiperSlide key={index}>
                                {isclient
                                        ?
                                        (
                                            <div className=" flex flex-wrap pb-8 lg:items-center po">
                                                <div className="w-full lg:w-3/12">
                                                    <div className="">
                                                        <AspectRatio ratio={1 / 1}>
                                                            <Image className="w-full rounded-lg h-full object-cover" src={swiperitem.image} alt={swiperitem.title} width={200} height={200}></Image>
                                                        </AspectRatio>
                                                    </div>
                                                </div>
                                                <div className=" w-full lg:w-9/12 max-md:py-16 pl-10 lg:pl-6 xl:pl-28 ">
                                                    <div className="relative before:absolute before:content-[''] before:top-0 before:left-0 before:w-20px before:h-20px before:bg-[url('../../public/images/colan.png')] before:bg-cover">
                                                        <div className="text-[21px]  2xl:text-[28px] leading-[1.5] tracking-[0.34px] 2xl:tracking-[0.45px] font-montsemibolditalic mb-4">
                                                            {swiperitem.description}
                                                        </div>
                                                        <div className=" text-successDark text-base font-montbook tracking-[0.27px]">
                                                            {"Nom du client - "}{swiperitem.title}
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                        :
                                        (
                                            <div className="pb-8 flex flex-wrap items-center pt-8 lg:pt-14 xl:pt-16 3xl:pt-20">
                                                <div className="w-full lg:w-[calc(100%-450px)] pb-10 lg:pb-0 lg:pr-16">
                                                    <div className="w-full h-full">
                                                        {swiperitem.attributes.media.data?
                                                            <>
                                                                {swiperitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                                    index2==0 ?
                                                                        <AspectRatio ratio={824 / 517}>
                                                                            <Image className="w-full h-[auto]" src={imageitem.attributes.url} alt={imageitem.attributes.alternativeText} key={index2} fill></Image>
                                                                        </AspectRatio>
                                                                    :
                                                                        ""
                                                                ))}
                                                            </>
                                                        :
                                                            <AspectRatio ratio={4 / 3}>
                                                                <Image className="w-full h-[auto]" src={swiperitem.image} alt={swiperitem.title} fill></Image>
                                                            </AspectRatio>
                                                        }
                                                    </div>
                                                </div>
                                                <div className="w-full lg:w-[450px]">
                                                    <div className="pb-5">
                                                        {swiperitem.title ?
                                                            <Title title={swiperitem.title} className="text-xl md:text-2xl lg:text-3xl xl:text-4xl text-danger2 " level={3} isHTML></Title>
                                                        :
                                                            <Title title={swiperitem.attributes.title} className="text-xl md:text-2xl lg:text-3xl xl:text-4xl text-danger2 " level={3} isHTML></Title>
                                                        }
                                                    </div>
                                                    <div className="text-sm 2xl:text-base font-montbook">
                                                        {swiperitem.description ?
                                                            <p>{swiperitem.description}</p>
                                                        :
                                                            <p>{swiperitem.attributes.description}</p>
                                                        }
                                                    </div>
                                                    {swiperitem.link ?
                                                        <Button className="mt-5" label={swiperitem.link} href={swiperitem.image} variant="transparent"></Button>
                                                    :
                                                        <Button className="mt-5" label={"Découvrir le projet"} href={` project/${swiperitem.attributes.slug}`} variant="transparent"></Button>
                                                    }
                                                </div>
                                            </div>
                                        )

                                }
                            </SwiperSlide>
                        ))}
                        {isclient ? (
                            <div className=" w-full lg:w-9/12 pl-10 lg:pl-6 xl:pl-28 ml-auto">
                            <div className="flex test-swiper items-center bg-muted5 py-3.5 px-7 rounded-md w-fit mx-auto lg:m-0 shadow-[0_3px_20px_rgb(0,0,0,16)]">
                                <div className="swiper-pagination mr-9"></div>
                                <div className="swiper-btn-wrapper flex items-center">
                                    <div className="swiper-button-prev after:hidden rotate-180 opacity-100 mr-2.5 h-fit">
                                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                            <title>right-arrow-svg</title>
                                            <defs>
                                                <image  width="512" height="258" id="im20g1" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                            </defs>
                                            <style>
                                            </style>
                                            <use id="img1" href="#im20g1" x="0" y="11"/>
                                        </svg>
                                    </div>
                                    <div className="swiper-button-next after:hidden opacity-100 h-fit">
                                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                            <title>right-arrow-svg</title>
                                            <defs>
                                                <image  width="512" height="258" id="i40mg1" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                            </defs>
                                            <style>
                                            </style>
                                            <use id="img1" href="#i40mg1" x="0" y="11"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                            </div>
                        ) : (
                            <div className="flex items-center bg-muted5 py-3.5 px-7 rounded-md w-fit mx-auto lg:m-0 shadow-[0_3px_20px_rgb(0,0,0,16)]">
                                <div className="swiper-pagination mr-9"></div>
                                <div className="swiper-btn-wrapper flex items-center">
                                    <div className="swiper-button-prev after:hidden rotate-180 opacity-100 mr-2.5 h-fit">
                                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                            <title>right-arrow-svg</title>
                                            <defs>
                                                <image  width="512" height="258" id="img001" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                            </defs>
                                            <style>
                                            </style>
                                            <use id="img1" href="#img001" x="0" y="11"/>
                                        </svg>
                                    </div>
                                    <div className="swiper-button-next after:hidden opacity-100 h-fit">
                                        <svg version="1.2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 280" width="512" height="280">
                                            <title>right-arrow-svg</title>
                                            <defs>
                                                <image  width="512" height="258" id="im4g100" href="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAgAAAAECCAMAAAC/hp9eAAAAAXNSR0IB2cksfwAAAY9QTFRF08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/08T/8ZCR/wAAAIV0Uk5TAAUqRmOAhUQHU9z/9rdnAjbEthFq/t0wOfpSFuT7VAG+dwz5IwjqVc2kKJIT7n2BglaDhIb8h1eIiYqLWIyNjo9ZkJGTWpSVlidcmJnzQrzwWwo42xr1wGUS8hdJfK+14+fpUBh6aAPJvfcg7bNgEEN2prBrYf1IIc6sJHjCxzsNT3teQWjmfikAAARBSURBVHic7dpnT1RRFEbhY0HFsaKIqOOggCKIDXsv2MXeu2LvvXf94fYACjPRZO5O7lrPd5KdrJdMBk5KijJs+IiRNaNGj4m+QzFqxxZ+GDd+wsToW5S5SZMLfeqmRJ+jjE2tL/Q3rSH6IGVqemNhoHEzok9ShmbOKvypODv6KGWm1PRXfxdA0jBIfxcAMmfQAbgAjME+AVwAx9zmIQbgAhhahurvAhhahx6AC0CYV24B86OvU9W1lRmACwBYUG4ALiD/2osugK2j7ABcQO4t7HQBbBPKD8AF5N4iFwBX4wLgXACdC6BzAXSLXQCcC6BzAXQugG6JC4BzAXQVF7A0+kJVlwugW+YC4FwAnQugcwF0XS4AzgXQuQA6F0A3ygXAuQA6F0DnAuiWV1jACheQcy6AzgXQuQC6lZUWsCr6QlWXC6BzAXQugM4F0K12AXAugM4F0LkAujUuAM4F0LkAOhdAt9YFwLkAOhdA5wLo1lVawProC1VdLoDOBdC5ALoNLgDOBdC5ADoXQOcC6Da6ADgXQOcC6FwA3SYXAOcC6FwAnQug2+wC4CouYEv0haouF0DnAui2ugA4F0DnAuhcAJ0LoNvmAuBcAJ0LoHMBdB0uAM4F0LkAOhdA1+0C4CouYHv0haouF0DnAuhcAN0OFwDnAuhcAJ0LoNvpAuBcAJ0LgCu5ALjSLhfA5gLoXADdfyygtHvPxr3Ki309/7aA/QcOVvgB5cyKQ335D3fXR5+jzPUt4MjR6FsU4fc7wWOd0ZcoxvGWHwOoi75DUU6UvvU/GX2F4kxN6dTp6CMUp+dMOht9gyLVpnPRJyhSVzoffYIiXUgXo09QpEvpcvQJinTJvwKwXUjLok9QpK7UG32CItWmK83RNyhOz5mUrkYfoTjXUkrXb0RfoSg3v/8zKLXfir5DMXpu/3wQ0Hsn+hJFKM74/SToyt3oW5S94ux+j0LvjY0+Rxkb0P+b+w8ePnqs3Kj0LPzP/sqXGvuj2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/Nvuz2Z/N/mz2Z7M/m/3Z7M9mfzb7s9mfzf5s9mezP5v92ezPZn82+7PZn83+bPZnsz+b/dnsz2Z/tif2R5veaH+ymbPsT1Zqsj9ag/3Z5tifrewngP1zb26z/dFa7M/Wan+4efZna7M/2wL7s7UX7c/WYX+2hZ32Z3v6zP5sz+8M6N+4PvogZezFy379X72OPkeZa33z9teXgXfvo29RjA8f1376/CX2t/8r10/hmgpWx5IAAAAASUVORK5CYII="/>
                                            </defs>
                                            <style>
                                            </style>
                                            <use id="img1" href="#im4g100" x="0" y="11"/>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        )
                    }
                    </Swiper>
                }

            </div>
        </>
    );
};
export default SwiperItem;