import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";
import { AspectRatio } from "@/components/ui/aspect-ratio";

interface Props {
    className?: string;
    visiondata?: any;
}

function Visioncard({
    className, visiondata, 
}: Props) {
    return (
        <>
            <div className={` ${className}`} key={visiondata.id}>
                <div className="text-center md:text-left w-full lg:w-1/2 lg:pr-9 xl:pr-16 2xl:pr-24 3xl:pr-24 md:pb-7 lg-pb-0">
                    <Title title={visiondata.title} Iswhite className="pb-5 font-montbold text-xl md:text-2xl lg:text-3xl xl:text-4xl" level={3} isHTML></Title>
                    <div className="pb-5">
                        <p className="font-montbook text-success">{visiondata.description}</p>
                    </div>
                    <div className="flex flex-wrap md:-mx-2">
                        {visiondata.keyData.map((visionitem: {
                            title: any;
                            value: any;
                        })=>(
                            <>
                                <div className="w-full md:w-1/3 pb-7 md:pb-0 md:px-2">
                                    <div className="text-success text-xs leading-7 font-montsemibold">{visionitem.title}</div>
                                    <Title title={visionitem.value} Iswhite className="font-montbold text-lg 2xl:text-xl 3xl:text-2xl leading-7" level={4} isHTML></Title>
                                </div>
                            </>
                            ))
                        }
                    </div>
                </div>
                <div className="w-full lg:w-1/2">
                    <AspectRatio ratio={4 / 3}>
                        <Image src={visiondata.image.data.attributes.url} fill={true} alt={visiondata.image.data.attributes.alternativeText} />
                    </AspectRatio>
                </div>
            </div>
        </>
    );
}

export default Visioncard;