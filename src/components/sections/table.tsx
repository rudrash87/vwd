import React, { type ReactNode } from "react";
import Title from "@/components/ui/title";

interface Props {
    className?: string;
    title:any;
    level: any;
    text: any;
    tabledata?: any;
}

function TableData({
    className, title, level, text, tabledata,
}: Props) {
    return (
        <>
            <Title title={title} Iswhite className="uppercase font-montbold text-xl md:text-2xl 2xl:text-[27px]" level={level} isHTML></Title>
            {text&&(
                <div className="pb-6 text-xs text-[#58656a]"><p>{text}</p></div>
            )}
            {tabledata.map((tabledataitem: {
                id: any;
                label: any;
                monthly: any;
                hourly: any;
            }, index: React.Key | null | undefined) => (
                <div className={`flex flex-wrap border-t py-5 border-muted7 border-solid`} key={tabledataitem.id}>
                    <div className="w-full py-2 md:-py-0 md:w-1/5 md:pr-4 text-lg font-montsemibold text-success">{tabledataitem.label}</div>
                    <div className="w-full py-2 md:-py-0 md:w-2/5 md:pl-4 text-lg bg-gradient-to-r from-[#ac93bb] via-[#826ba5] to-[#5435AB] text-transparent bg-clip-text font-montsemibold">{tabledataitem.monthly}<span className="text-xs font-montbook text-[#58656a]"> / month</span></div>
                    <div className="w-full py-2 md:-py-0 md:w-2/5 md:pl-4 text-lg font-montsemibold text-success">{tabledataitem.hourly}<span className="text-xs font-montbook text-[#58656a]"> / additional hour</span></div>
                </div>
            ))
            }
        </>
    );
}

export default TableData;