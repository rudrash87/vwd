import React, { type ReactNode } from "react";
import { useState } from "react";
import { BlogcatData, BlogListingData } from "@/data/blogpage-data";
interface Props {
    className?: string;
    blogdata?: any;
}

function FilterBlog({
    className, blogdata
}: Props){
    const [selectedGenre, setSelectedGenre] = useState("");

    return (
        <>
            <div className={' ${className}'}>
                <div className="col-span-1 pb-8 md:pb-10 xl:pb-[70px] w-fit m-auto max-md:px-2.5">
                    <BlogcatData catsele={selectedGenre} cselect={setSelectedGenre} />
                </div>
                <div className="col-span-3">
                    <BlogListingData catsele={selectedGenre} />
                </div>
            </div>
        </>
    );
};
export default FilterBlog;