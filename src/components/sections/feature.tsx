import React, { type ReactNode } from "react";
import Image from 'next/image'
import Title from "@/components/ui/title";

interface Props {
    className?: string;
    featuredata?: any;
    varient?: any;
}

function Feature({
    className, featuredata, varient, 
}: Props) {
    const getVariant = () => {
    switch (varient) {
        case "grid4":
            return "icon-card-4 w-full md:w-1/2 lg:w-1/3 xl:w-1/4";
        case "grid3":
            return "w-full md:w-1/2 lg:w-1/3";
        default:
            return "transparent";
    }
    };
    const type_link = getVariant();
    return (
        <>
                {varient == 'grid4' ?
                    (
                        <div className={` ${className} flex flex-wrap text-center md:text-left`}>
                            {featuredata.map((textimageitem: {
                                link: string;
                                image: any;
                                icon:any;
                                title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                                <div className={`px-2 max-md:py-2 text-center ${type_link}`} key={index}>
                                    <div className=" bg-backgroundDark px-5 md:px-6 py-9 h-full">
                                        <div className="pb-4">
                                            <div className="h-16 w-16 mx-auto p-3 rounded-md flex bg-black ">
                                                {textimageitem.icon ?
                                                    <Image className="w-full !h-full object-contain m-auto object-cover" src={textimageitem.icon.data.attributes.url} height={200} width={150} alt={textimageitem.icon.data.attributes.alternativeText}></Image>
                                                :
                                                    <Image className="w-full !h-full object-contain m-auto object-cover" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                                }
                                            </div>
                                        </div>
                                        <div className="">
                                            <Title title={textimageitem.title} className="pb-4 text-[22px] leading-[1]" level={5} isHTML></Title>
                                            <div className="">
                                                <p className="text-sm 2xl:text-base text-success font-montbook tracking-[0.26px]">{textimageitem.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                            }
                        </div>
                    )
                :
                    <>
                    {varient == 'grid3-center' ?
                    (
                        <div className={` ${className} flex flex-wrap text-center md:-mx-6`}>
                            {featuredata.map((textimageitem: {
                                link: string;
                                image: any;
                                title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                                <div className={`w-full md:w-1/2 lg:w-1/3 md:px-6 py-9 ${type_link}`} key={index}>
                                    <div className="h-full text-center">
                                        <div className="h-20 w-20 mx-auto mb-4 p-4 rounded-md flex bg-muted3 ">
                                            <Image className="w-full h-full object-contain m-auto object-cover" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                        </div>
                                        <div className="">
                                            <Title title={textimageitem.title} className="pb-4 text-xl xl:text-2xl font-montbold" level={5} isHTML></Title>
                                            <div className="">
                                                <p className="text-sm 2xl:text-base text-success font-montbook tracking-[0.26px]">{textimageitem.description}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                            }
                        </div>
                    )
                :
                    <>
                    {varient == 'grid3-left' ?
                        (
                            <div className={` ${className} flex flex-wrap md:text-left md:-mx-6`}>
                                {featuredata.map((textimageitem: {
                                    link: string;
                                    image: any;
                                    title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                                    <div className={`w-full md:w-1/2 lg:w-1/3 md:px-6 py-9 ${type_link}`} key={index}>
                                        <div className="h-full text-center md:text-left">
                                            <div className="h-20 w-20 mx-auto md:mx-0 mb-4 p-4 rounded-md flex bg-muted3 ">
                                                <Image className="w-full h-full object-contain m-auto object-cover" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                            </div>
                                            <div className="">
                                                <Title title={textimageitem.title} className="pb-4 text-xl xl:text-2xl font-montbold" level={5} isHTML></Title>
                                                <div className="">
                                                    <p className="text-sm 2xl:text-base text-success font-montbook tracking-[0.26px]">{textimageitem.description}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                                }
                            </div>
                        )
                    :
                        <>
                        {varient == 'grid2-icon' ?
                            (
                                <div className={` ${className} flex flex-wrap md:-mx-6 pt-9`}>
                                    {featuredata.map((textimageitem: {
                                        link: string;
                                        image: any;
                                        title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                                        <div className={`w-full md:w-1/2 md:px-6 py-9 ${type_link}`} key={index}>
                                            <div className="h-full flex flex-wrap">
                                                <div className="h-20 w-20 mx-auto md:mx-0 mb-4 p-4 rounded-md flex bg-muted3 ">
                                                    <Image className="w-full h-full object-contain m-auto object-cover" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                                </div>
                                                <div className="w-[calc(100%-80px)] pl-7">
                                                    <Title title={textimageitem.title} className="pb-2" level={5} isHTML></Title>
                                                    <div className="">
                                                        <p className="text-sm 2xl:text-base text-success font-montbook tracking-[0.26px]">{textimageitem.description}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    ))
                                    }
                                </div>
                            )
                        :
                            (
                            <div className={` ${className} flex flex-wrap text-center md:text-left md:-mx-6`}>
                                {featuredata.map((textimageitem: {
                                    link: string;
                                    image: any;
                                    icon: any;
                                    title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; },index: React.Key | null | undefined) => (
                                    <div className={`md:px-6 py-9 ${type_link}`} key={index}>
                                        <div className="h-full">
                                            <div className="h-20 w-20 mx-auto mb-4 md:mx-0 p-4 rounded-md flex bg-muted3 ">
                                                {textimageitem.icon ?
                                                    <Image className="w-full h-full object-contain m-auto object-cover" src={textimageitem.icon.data.attributes.url} height={200} width={150} alt={textimageitem.icon.data.attributes.alternativeText}></Image>
                                                :
                                                    <Image className="w-full h-full object-contain m-auto object-cover" src={textimageitem.image} height={200} width={150} alt={textimageitem.title}></Image>
                                                }
                                            </div>
                                            <div className="">
                                                <Title title={textimageitem.title} className="pb-4 text-xl xl:text-2xl font-montbold" level={5} isHTML></Title>
                                                <div className="">
                                                    <p className="text-sm 2xl:text-base text-success font-montbook tracking-[0.26px]">{textimageitem.description}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                ))
                                }
                            </div>
                            )
                        }
                        </>
                    }
                    </>
                    
                }
                </>
            }
        </>
    );
}

export default Feature;