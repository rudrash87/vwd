import React, { useState } from "react";
import Image from 'next/image'
import { AspectRatio } from "@/components/ui/aspect-ratio";

interface Props {
    className?: string;
    portfoliodata?: any;
    variant?: any;
}

function Portfolio({
    className, portfoliodata, variant, 
}: Props) {
    return (
        variant=="technology"?
            <>
                <div className={`section-${variant} ${className}`}>
                    <div className=" flex flex-wrap ">
                        <div className=" flex flex-wrap w-full md:w-3/6 ">
                            {portfoliodata.map((portfolioitem: any,index: any)=>(
                                    index < 2 ? (
                                        <div className="w-full md:w-1/2 w-25 px-0 py-2.5 sm:p-2.5 flex-auto" key={index}>
                                            <div className=" h-full rounded-md overflow-hidden"> 
                                                <a className="transition-32s" href={portfolioitem.url}>
                                                    <AspectRatio ratio={188 / 109}>
                                                        <Image className="w-full object-center object-cover  transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt="name"></Image>
                                                    </AspectRatio>
                                                </a>
                                            </div>
                                        </div>
                                    ) : (
                                        ""
                                    )
                            ))}
                                
                            {portfoliodata.map((portfolioitem: any,index: any)=>(
                                    index == 2 ? (
                                        <div className="w-full md:w-1/2 w-50 px-0 py-2.5 sm:p-2.5 flex-auto" key={index}>
                                            <div className=" h-full rounded-md overflow-hidden"> 
                                                <a className="transition-32s" href={portfolioitem.url}>
                                                    <AspectRatio ratio={188 / 109}>
                                                        <Image className="w-full object-center object-cover  transition-32s scale-100 hover:scale-105  " src={portfolioitem.src} fill alt="name"></Image>
                                                    </AspectRatio>
                                                </a>
                                            </div>
                                        </div>
                                    ) : (
                                        ""
                                    )
                            ))}
                        </div>
                        <div className=" flex flex-wrap w-full md:w-3/6 ">
                            {portfoliodata.map((portfolioitem: any,index: any)=>(
                                index == 3 ? (
                                    <div className="w-full item flex-auto px-0 py-2.5 sm:p-2.5"> 
                                        <div className=" h-full rounded-md overflow-hidden"> 
                                            <a href={portfolioitem.url}>
                                                <Image className="w-full object-center object-cover  transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt="name"></Image>
                                            </a>
                                        </div>
                                    </div>
                                ) : (
                                    ""
                                )
                            ))}   
                        </div>
                    </div>
                    <div className="flex flex-wrap">
                        {portfoliodata.map((portfolioitem: any,index: any)=>(
                            index > 3 ? (
                                <div className="w-full sm:w-3/6 md:w-1/3 flex-auto px-0 py-2.5 sm:p-2.5  w-33" key={index}>
                                    <div className=" h-full rounded-md overflow-hidden"> 
                                        <a className="transition-32s" href={portfolioitem.url}>
                                            <Image className="w-full object-center object-cover  transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt="name"></Image>
                                        </a>
                                    </div>
                                </div>
                            ) : (
                                ""
                            )
                        ))}
                    </div>
                </div>
            </>
        :
        variant=="realisations"?
        <>
            <div className={`section-${variant} ${className}`}>
                {portfoliodata.map((portfolioitem: any,index: any)=>(
                    index % 6 == 0 ? (
                        <div className="w-full sm:w-1/2 p-1.5" key={index}>
                            <div className="w-full">
                                <div className="w-full overflow-hidden rounded-md">
                                    {portfolioitem.attributes ?
                                        <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                            {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                index2 == 0 && (
                                                    <AspectRatio ratio={188 / 109}>
                                                        <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                    </AspectRatio>
                                                )
                                            ))}
                                        </a>
                                    :
                                        <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                            <AspectRatio ratio={188 / 109}>
                                                <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                            </AspectRatio>
                                        </a>
                                    }
                                </div>
                            </div>
                        </div>
                    )  : (
                        index % 6 == 1 ? (
                            <div className="w-full sm:w-1/2 p-1.5" key={index}>
                                <div className="w-full">
                                    <div className="w-full overflow-hidden rounded-md">
                                        {portfolioitem.attributes ?
                                            <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                    index2 == 0 && (
                                                        <AspectRatio ratio={188 / 109}>
                                                            <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                        </AspectRatio>
                                                    )
                                                ))}
                                            </a>
                                        :
                                            <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                <AspectRatio ratio={188 / 109}>
                                                    <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                </AspectRatio>
                                            </a>
                                        }
                                    </div>
                                </div>
                            </div>
                        ) : (
                            index % 6 == 2 ? (
                                <div className="w-full sm:w-1/3 p-1.5" key={index}>
                                    <div className="w-full">
                                        <div className="w-full overflow-hidden rounded-md">
                                            {portfolioitem.attributes ?
                                                <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                    {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                        index2 == 0 && (
                                                            <AspectRatio ratio={623 / 360}>
                                                                <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                            </AspectRatio>
                                                        )
                                                    ))}
                                                </a>
                                            :
                                                <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                    <AspectRatio ratio={623 / 360}>
                                                        <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                    </AspectRatio>
                                                </a>
                                            }
                                        </div>
                                    </div>
                                </div>
                            ) : (
                                index % 6 == 3 ? (
                                    <div className="w-full sm:w-1/3 p-1.5" key={index}>
                                        <div className="w-full">
                                            <div className="w-full overflow-hidden rounded-md">
                                                {portfolioitem.attributes ?
                                                    <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                        {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                            index2 == 0 && (
                                                                <AspectRatio ratio={623 / 360}>
                                                                    <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                                </AspectRatio>
                                                            )
                                                        ))}
                                                    </a>
                                                :
                                                    <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                        <AspectRatio ratio={623 / 360}>
                                                            <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                        </AspectRatio>
                                                    </a>
                                                }
                                            </div>
                                        </div>
                                    </div>
                                ) : (
                                    index % 6 == 4 ? (
                                        <div className="w-full sm:w-1/3 p-1.5" key={index}>
                                            <div className="w-full">
                                                <div className="w-full overflow-hidden rounded-md">
                                                    {portfolioitem.attributes ?
                                                        <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                            {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                                index2 == 0 && (
                                                                    <AspectRatio ratio={623 / 360}>
                                                                        <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                                    </AspectRatio>
                                                                )
                                                            ))}
                                                        </a>
                                                    :
                                                        <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                            <AspectRatio ratio={623 / 360}>
                                                                <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                            </AspectRatio>
                                                        </a>
                                                    }
                                                </div>
                                            </div>
                                        </div>
                                    ) : (
                                        index % 6 == 5 ? ( 
                                            <div className="w-full p-1.5" key={index}>
                                                <div className="w-full">
                                                    <div className="w-full overflow-hidden rounded-md">
                                                        {portfolioitem.attributes ?
                                                            <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                                {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                                    index2 == 0 && (
                                                                        <AspectRatio ratio={316 / 135}>
                                                                            <Image className="w-full object-center object-cover  transition-32s scale-100      hover:scale-105  " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                                        </AspectRatio>
                                                                    )
                                                                ))}
                                                            </a>
                                                        :
                                                            <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                                <AspectRatio ratio={316 / 135}>
                                                                    <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                                </AspectRatio>
                                                            </a>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        ) : (
                                            <div className="w-full sm:w-1/2 p-1.5" key={index}>
                                                <div className="w-full">
                                                    <div className="w-full overflow-hidden rounded-md">
                                                        {portfolioitem.attributes ?
                                                            <a className=" transition-32s" href={`/project/${portfolioitem.attributes.slug}`}>
                                                                {portfolioitem.attributes.media.data.map((imageitem:any,index2:any)=>(
                                                                    index2 == 0 && (
                                                                        <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={imageitem.attributes.url} fill alt={imageitem.attributes.alternativeText} key={index2}></Image>
                                                                    )
                                                                ))}
                                                            </a>
                                                        :
                                                            <a className=" transition-32s" href={`/project/${portfolioitem.url}`}>
                                                                <AspectRatio ratio={188 / 109}>
                                                                    <Image className="w-full object-center object-cover rounded-md transition-32s scale-100 hover:scale-105 " src={portfolioitem.src} fill alt={portfolioitem.src}></Image>
                                                                </AspectRatio>
                                                            </a>
                                                        }
                                                    </div>
                                                </div>
                                            </div>
                                        )
                                    )
                                )
                            )
                        )
                    )
                ))}
            </div>
        </>
        :
            variant=="projectdetails"?
                <>
                    <div className={`section-${variant} ${className}`}>
                        {portfoliodata.map((portfolioitem: any,index: any)=>(
                            index  == 0 && (
                                <div className="w-full sm:w-9/12 p-2.5 overflow-hidden" key={index}>
                                    <a className="block h-full rounded-md overflow-hidden" href={portfolioitem.url}>
                                        <Image className="overflow-hidden rounded-md transition-32s scale-100 hover:scale-105 block h-full object-cover w-full rounded-md" src={portfolioitem.src} fill alt="name"></Image>                                                    
                                    </a>
                                </div>
                                )
                        ))}
                        <div className="w-full sm:w-3/12">
                            {portfoliodata.map((portfolioitem: any,index: any)=>(
                                index  != 0 && (
                                    <div className="w-full p-2.5 overflow-hidden" key={index}>
                                        <a className="block rounded-md overflow-hidden" href={portfolioitem.url}>
                                            <Image className="overflow-hidden rounded-md transition-32s scale-100 hover:scale-105 block w-full rounded-md" src={portfolioitem.src} fill alt="name"></Image>
                                        </a>
                                    </div>
                                    )
                            ))}
                        </div>
                    </div>                                           
                </>
            :
            (
                <div className={`section-${variant} ${className}`}>
                    {portfoliodata.map((portfolioitem: any,index: any)=>(
                        <div className="portfolio-item" key={index}>
                            <div className="image-link-wrapper">
                                <a href={portfolioitem.url}>
                                    <AspectRatio ratio={188 / 109}>
                                        <Image src={portfolioitem.src} fill alt="name"></Image>
                                    </AspectRatio>
                                </a>
                            </div>
                        </div>  
                    ))}
                </div>
            )
    );
}

export default Portfolio;