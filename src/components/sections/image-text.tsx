import React, { type ReactNode } from "react";
import Image from 'next/image'
import Icons from "./icons";
import Title from "@/components/ui/title";
import Button from "@/components/ui/button";
import { AspectRatio } from "@/components/ui/aspect-ratio";

interface Props {
    className?: string;
    textimage?: any;
    tvarient?: any;
    icons?: any;
}

function Imagetext({
    className, textimage, tvarient, icons
}: Props) {
    return (
        <>
            {textimage.map((textimageitem: {
                link: string;
                image: any;
                toptitle: any;
                position: any;
                level: number,
                subtitle: any;
                multicolor: any;
                title: string | number | boolean | any | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined;
                description: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined;
                linkText:any;
                linkUrl:any;
                newWindow: boolean;
                mediaPosition: any;
                media: any;
                 },index: React.Key | null | undefined) => (
                <div className=" py-14 md:py-16 lg:py-20 xl:py-28 2xl:py-36 " key={index}>
                    <div className="container">
                       <div className={`flex flex-wrap items-center sm:-mx-10 lg:-mx-16 2xl:-mx-20 ${textimageitem.mediaPosition=="right"? "flex-row" : "flex-row-reverse"}`}>
                            <div className="w-full md:w-6/12 sm:px-10 lg:px-16 2xl:px-20 max-md:pb-8">
                                {textimageitem.toptitle? (
                                        <div className="text-lg 2xl:text-xl text-danger2 font-bold pb-3.5">{textimageitem.toptitle}</div>
                                    ): ""
                                }
                                <Title title={textimageitem.title} Multicolor={textimageitem.multicolor} className={`uppercase font-montbold text-xl md:text-2xl lg:text-3xl 2xl:text-4xl pb-5 ${tvarient && "text-danger2"}`} level={3} isHTML></Title>
                                <div className=" mb-7">
                                    <p className="text-base font-montbook tracking-[0.26px] text-success leading-[1.75]">{textimageitem.description}</p>
                                </div>
                                {textimageitem.linkUrl? (
                                    <div><Button newwindow={textimageitem.newWindow} className="link-two text-info hover:text-danger transition-32s font-montsemibold text-base tracking-[0.26px]" label={textimageitem.linkText} href={textimageitem.linkUrl} variant="without-border"></Button></div>
                                ): (
                                    ""
                                )}

                                {icons ?
                                    <>
                                        <Icons textimage={icons} className="3grid"></Icons>
                                    </>
                                :
                                    ""
                                }
                            </div>
                            <div className="w-full md:w-6/12 sm:px-10 lg:px-16 2xl:px-20">
                                <div className=" max-md:pb-5">
                                    {textimageitem.media ?
                                        textimageitem.media.data ?
                                        <AspectRatio ratio={212 / 165}>
                                            <Image className="rounded-lg overflow-hidden" src={textimageitem.media.data.attributes.url} fill={true} alt={textimageitem.media.data.attributes.alternativeText}></Image>
                                        </AspectRatio>
                                        : 
                                        <AspectRatio ratio={212 / 165}>
                                            <Image className="rounded-lg overflow-hidden" src={textimageitem.image} fill={true} alt={textimageitem.title}></Image>
                                        </AspectRatio>
                                        :
                                        <AspectRatio ratio={212 / 165}>
                                            <Image className="rounded-lg overflow-hidden" src={textimageitem.image} fill={true} alt={textimageitem.title}></Image>
                                        </AspectRatio>
                                    }
                                </div>                            
                            </div>
                        </div>
                    </div>
                </div>
            ))}
        </>
    );
}

export default Imagetext;