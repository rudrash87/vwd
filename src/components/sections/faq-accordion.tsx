"use client"
import React, { type ReactNode } from "react";
import { Accordion, AccordionItem } from "@nextui-org/accordion";
import Plus from "../icons/plus";
import Minus from "../icons/minus";

interface Props {
  faqs?: any;
  className?: string;
  isHTML?: boolean;
}
export type AccordionItemClassnames = {
  base?: string;
  heading?: string;
  trigger?: string;
  titleWrapper?: string;
  title?: string;
  subtitle?: string;
  startContent?: string;
  indicator?: string;
  content?: string;
};

function FaqAccordion({
  faqs, className, isHTML = true,
}: Props) {
  const itemClasses = {
    base: "py-0 px-0 w-full",
    title: "font-normal text-medium",
    trigger: "py-0 data-[hover=true]:bg-default-100 rounded-lg h-14 flex items-center flex-row-reverse justify-end ",
    indicator: "text-medium",
    content: "text-small",
  };
  return (
    <>
      <Accordion showDivider={false} itemClasses={itemClasses}>
        {faqs.map((faq: { id: React.Key | null | undefined; Question: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; Answser: string | number | boolean | React.ReactElement<any, string | React.JSXElementConstructor<any>> | Iterable<React.ReactNode> | React.ReactPortal | React.PromiseLikeOfReactNode | null | undefined; }) => (
          <AccordionItem key={faq.id} className={`${className}`} indicator={({ isOpen }) => (isOpen ? <Minus /> : <Plus />)} title={faq.Question}>
            <div className="text-sm 2xl:text-base font-montregular pl-[79px]">{faq.Answser}</div>
          </AccordionItem>
        ))}
      </Accordion>
    </>
  );
}

export default FaqAccordion;