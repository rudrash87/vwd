"use client";
import { useState } from "react";
import Navbar from "./desktopmenu";
import Sidebar from "./mobilemenu";

const Navigation = (Props: any) => {
  // toggle sidebar
  const menudata = Props.menudata;
  const topcta = Props.topcta;
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => {
    setIsOpen(!isOpen);
  };
  return (
    <>
      <Navbar menudata={menudata} topcta={topcta} toggle={toggle} />
      <Sidebar menudata={menudata} topcta={topcta} isOpen={isOpen} toggle={toggle} />
    </>
  );
};

export default Navigation;