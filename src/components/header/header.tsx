import Image from 'next/image'
import TopmenuJson from '@/data/topmenu-fetch'
import Link from 'next/link'
import { getMegamenu } from '@/data/faker-data'

const Header = () => {
  const menudata = getMegamenu(3);
  return (
    <section className='flex justify-between relative'>
        <div className='flex items-center'>
            <Link href="/" className="hover:underline">
                <Image className="relative" src="/Logo.png" alt="Verywell Digital Logo" width={180} height={37} priority />
            </Link>
        </div>
        <div className='flex'>
            <TopmenuJson/>
        </div>
    </section>
  )
}

export default Header