import React from "react";
import Title from "@/components/ui/title";
import Image from "next/image";
import Link from "next/link";
import * as NavigationMenu from "@radix-ui/react-navigation-menu";
import { CaretDownIcon } from '@radix-ui/react-icons';
import { AspectRatio } from "@/components/ui/aspect-ratio";
import { getImageText } from "@/data/faker-data";
import Button from "@/components/ui/button";

function Navbar(Props: any, { toggle }: { toggle: () => void; }) {
  const menudata = Props.menudata;
  const topcta = Props.topcta;
  const imagetext = getImageText(2);
  return (
    <>
      <NavigationMenu.Root className="hidden xl:flex main-menu z-[1] items-center justify-center">
        <NavigationMenu.List className="m-0 p-0 flex list-none pr-20 3xl:pr-44">
          {/* <NavigationMenu.Item className="flex items-center">
            <NavigationMenu.Trigger className="flex items-center text-success hover:text-danger transition-32s font-montsemibold text-sm leading-7 tracking-[0.48px]">
              Solutions <CaretDownIcon className="text-violet10 w-[28px] h-[28px] relative -top-[1px] transition-transform duration-[250] ease-in group-data-[state=open]:-rotate-180" aria-hidden />
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="data-[motion=from-start]:animate-enterFromLeft data-[motion=from-end]:animate-enterFromRight data-[motion=to-start]:animate-exitToLeft data-[motion=to-end]:animate-exitToRight absolute top-[100%] left-0 z-50 w-full pt-4">
              <div className="container">
                <div className="overflow-hidden shadow-[0_0px_20px_20px_rgba(0,0,0,0.81)] border border-muted2 border-solid rounded-lg bg-black bg-opacity-90 pt-12 px-12 px-8 xl:pl-16 2xl:pl-20 xl:pr-16 2xl:pr-20 xl:pt-16 2xl:pt-20 flex flex-wrap">
                  <div className="w-1/3 px-9">
                    <Title title="Créativité" className="font-montregular text-xl text-successDark p-0 m-0 tracking-[0.3px]" level={5} isHTML></Title>
                    <ul className="one m-0 list-none w-full">
                      {menudata.map((menuitem: any, index: any) => (
                        <li className="pt-3" key={index}>
                          <Link href={menuitem.url} className="text-sm sub-menu  transition-32s items-center flex flex-wrap leading-7 tracking-[0.48px] transition-32s hover:text-danger font-montsemibold">
                            <div className="bg-muted3 rounded-md relative h-12 w-12">
                              <Image className="h-full w-full absolute p-1.5 object-contain object-center" fill={true} src={menuitem.image} alt="name"></Image>
                            </div>
                            <div className="pl-4 h-full w-[calc(100%-48px)] leading-[normal]">
                              <span className="leading-[normal] text-sm font-montregular leading-7 tracking-[0.48px] text-successDark">{menuitem.title}</span>
                              <p className="leading-[normal] text-xs text-info transition-32s tracking-[0.38px]">{menuitem.description}</p>
                            </div>
                          </Link>
                        </li>
                      ))}
                    </ul>
                  </div>
                  <div className="z-[1] before:z-[-1] before:w-[1000%] before:h-full before:top-0 before:bottom-0 before:left-[-50%] before:right-0 before:bg-muted3 before:absolute  relative before:content-['']  w-full bg-opacity-80 py-10 rounded-br-lg rounded-bl-lg mt-8">
                    <Title title="Zoom sur..." className="px-9 pb-4 font-montregular text-xl text-successDark p-0 m-0 tracking-[0.3px]" level={5} isHTML></Title>
                    <div className="flex flex-wrap">
                      {imagetext.map((imagetext: any, index: any) => (
                        <div className="w-1/2 px-9" key={index}>
                          <Link href={imagetext.image} className="text-sm sub-menu  transition-32s items-center flex flex-wrap leading-7 tracking-[0.48px] transition-32s hover:text-danger font-montsemibold">
                            <div className="relative h-[130px] w-[210px]">
                              <Image className="h-full w-full rounded-md object-cover object-center" fill={true} src={imagetext.image} alt="name"></Image>
                            </div>
                            <div className="pl-4 h-full w-[calc(100%-210px)]">
                              <span className="text-sm font-montsemibold leading-7 tracking-[0.48px] text-successDark">{imagetext.title}</span>
                              <p className="text-xs font-montregular text-info transition-32s tracking-[0.38px]">{imagetext.description}</p>
                              <Button className="mt-2.5 text-sm text-info font-montsemibold hover:text-danger tracking-[0.22px] transition-32s font-montsemibold" label={"En savoir plus"} href={imagetext.image} variant="without-border"></Button>
                            </div>
                          </Link>
                        </div>
                      ))}
                    </div>
                  </div>
                </div>
              </div>
            </NavigationMenu.Content>
          </NavigationMenu.Item> */}
          
          {menudata.map((menuitem:any,index:any)=>(
            <>
              {menuitem.attributes.parentMenu.data ?
                ""
              :
                menuitem.attributes.link ? 
                  <NavigationMenu.Item className="pl-4 pr-4" key={index}>
                      <NavigationMenu.Link className="text-success transition-32s font-montsemibold text-sm leading-7 tracking-[0.48px] hover:text-danger" href={menuitem.attributes.link.url}>
                        {menuitem.attributes.title}
                      </NavigationMenu.Link>
                    </NavigationMenu.Item>
                :
                  <NavigationMenu.Item className="flex items-center" key={index}>
                    <NavigationMenu.Trigger className="flex items-center text-success hover:text-danger transition-32s font-montsemibold text-sm leading-7 tracking-[0.48px]">
                      {menuitem.attributes.title} <CaretDownIcon className="text-violet10 w-[28px] h-[28px] relative -top-[1px] transition-transform duration-[250] ease-in group-data-[state=open]:-rotate-180" aria-hidden />
                    </NavigationMenu.Trigger>
                    <NavigationMenu.Content className="data-[motion=from-start]:animate-enterFromLeft data-[motion=from-end]:animate-enterFromRight data-[motion=to-start]:animate-exitToLeft data-[motion=to-end]:animate-exitToRight absolute top-[100%] left-0 z-50 w-full pt-4">
                      <div className="container">
                        <div className="overflow-hidden shadow-[0_0px_20px_20px_rgba(0,0,0,0.81)] border border-muted2 border-solid rounded-lg bg-black bg-opacity-90 pt-12 px-12 px-8 xl:pl-16 2xl:pl-20 xl:pr-16 2xl:pr-20 xl:pt-16 2xl:pt-20 flex flex-wrap">
                          {menudata.map((menuitems:any,index1:any)=>(
                            menuitems.attributes.parentMenu.data &&
                              <>
                                {menuitem.id == menuitems.attributes.parentMenu.data.id &&
                                  <div className="w-1/3 px-9" key={index1}>
                                    <Title title={menuitems.attributes.title} className="font-montregular text-xl text-successDark p-0 m-0 tracking-[0.3px]" level={5} isHTML></Title>
                                    <ul className="one m-0 list-none w-full">
                                      {menudata.map((menusubitems:any,index2:any)=>(
                                        menusubitems.attributes.parentMenu.data &&
                                          menusubitems.attributes.parentMenu.data.attributes.parentMenu.data &&
                                            <>
                                              {menusubitems.attributes.parentMenu.data.id == menuitems.id &&
                                                <li className="pt-3" key={index2}>
                                                  <Link href={menusubitems.attributes.link.url} className="text-sm sub-menu  transition-32s items-center flex flex-wrap leading-7 tracking-[0.48px] transition-32s hover:text-danger font-montsemibold">
                                                    <div className="bg-muted3 rounded-md relative h-12 w-12">
                                                      {menusubitems.attributes.ico.data &&
                                                        <Image className="h-full w-full absolute p-1.5 object-contain object-center" fill={true} src={menusubitems.attributes.ico.data.attributes.url} alt={menusubitems.attributes.title}></Image>
                                                      }
                                                    </div>
                                                    <div className="pl-4 h-full w-[calc(100%-48px)] leading-[normal]">
                                                      {menusubitems.attributes.title &&
                                                        <span className="leading-[normal] text-sm font-montregular leading-7 tracking-[0.48px] text-successDark">
                                                          {menusubitems.attributes.title}
                                                        </span>
                                                      }
                                                      {menusubitems.attributes.description &&
                                                        <p className="leading-[normal] text-xs text-info transition-32s tracking-[0.38px]">
                                                          {menusubitems.attributes.description}
                                                        </p>
                                                      }
                                                    </div>
                                                  </Link>
                                                </li>
                                              }
                                            </>
                                      ))}
                                    </ul>
                                  </div>
                                }
                              </>
                          ))}
                        </div>
                      </div>
                    </NavigationMenu.Content>
                  </NavigationMenu.Item>
              }
            </>
          ))}
        </NavigationMenu.List>

        <NavigationMenu.List className="flex items-center">
          {topcta.map((items:any,index:any) => (
              items.id == 1 ?
                (
                  <NavigationMenu.Item className="pl-4 pr-4" key={index}>
                    <NavigationMenu.Link target={`${items.attributes.link.newWindow ? "_blank" : ""}`} className="text-danger transition-32s hover:text-white font-montsemibold text-sm leading-7 tracking-[0.48px]" href={items.attributes.link.url}>
                      {items.attributes.link.text}
                    </NavigationMenu.Link>
                  </NavigationMenu.Item>
                )
              :
                items.id == 2 &&
                  (
                    <NavigationMenu.Item className="pl-4" key={index}>
                      <NavigationMenu.Link target={`${items.attributes.link.newWindow ? "_blank" : ""}`} className="font-montsemibold text-sm tracking-[0.28px] text-danger py-4 px-6 rounded-md border-2 border-solid border-danger inline-block hover:text-muted2 hover:bg-danger transition-32s" href={items.attributes.link.url}>
                        {items.attributes.link.text}
                      </NavigationMenu.Link>
                    </NavigationMenu.Item>
                  )
          ))}
        </NavigationMenu.List>
      </NavigationMenu.Root>
    </>
  );
};

export default Navbar;