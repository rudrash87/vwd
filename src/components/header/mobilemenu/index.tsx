import Link from "next/link";
import Title from "@/components/ui/title";
import Image from "next/image";
import * as NavigationMenu from "@radix-ui/react-navigation-menu";
import { CaretDownIcon } from '@radix-ui/react-icons';
import React from "react";
import { useEffect } from 'react';
import { AspectRatio } from "@/components/ui/aspect-ratio";
import { getImageText } from "@/data/faker-data";
import Button from "@/components/ui/button";

type NewType = {
  isOpen: boolean;
  toggle: () => void;
  menudata: any;
  topcta: any;
};

function Sidebar({
  topcta, menudata, isOpen, toggle,
}: NewType): JSX.Element {
  const imagetext = getImageText(2);
  useEffect(() =>
    {
      {isOpen ?
        document.body.classList.add("open-menu")
      :
        document.body.classList.remove("open-menu")
      }
    }
  )
  return (
    <>
    {isOpen ?
      <button className="inline-flex items-center" onClick={toggle}>
      {/* Close icon */}
      <svg xmlns="http://www.w3.org/2000/svg"
        width="40"
        height="40"
        viewBox="0 0 24 24"
      >
        <path
          fill="currentColor"
          d="M19 6.41L17.59 5L12 10.59L6.41 5L5 6.41L10.59 12L5 17.59L6.41 19L12 13.41L17.59 19L19 17.59L13.41 12L19 6.41Z" />
      </svg>
      </button>
    :
    <button
      type="button"
      className="inline-flex xl:hidden items-center"
      onClick={toggle}
    >
      <svg
        xmlns="http://www.w3.org/2000/svg"
        width="40"
        height="40"
        viewBox="0 0 24 24"
      >
        <path
          fill="#fff"
          d="M3 6h18v2H3V6m0 5h18v2H3v-2m0 5h18v2H3v-2Z"
        />
      </svg>
    </button>
    }
      <div className={`"sidebar-container" ${isOpen} "fixed fixed pl-5 pr-5 w-full md:w-3/5 lg:w-1/2 h-full transition-32s overflow-hidden overflow-y-auto bg-background right-0 z-10"`}
        style={{
          opacity: `${isOpen ? "1" : "0"}`,
          top: ` ${isOpen ? "50px" : "50px"}`,
          right: ` ${isOpen ? "0px" : "-100%"}`,
          height: ` ${isOpen ? "100vh" : "0"}`,
        }}
      >
        <NavigationMenu.Root className="sidebar-nav leading-relaxed text-xl">
          <NavigationMenu.List className="menu-list">
            {menudata.map((menuitem:any,index:any)=>(
              <>
                {menuitem.attributes.parentMenu.data ?
                  ""
                :
                  menuitem.attributes.link ?
                    <NavigationMenu.Item className="mt-5" key={index}>
                      <NavigationMenu.Link className="font-montsemibold text-success text-2xl" href={menuitem.attributes.link.url}>
                        {menuitem.attributes.title}
                      </NavigationMenu.Link>
                    </NavigationMenu.Item>
                  :
                    <NavigationMenu.Item className="mt-5" key={index}>
                      <NavigationMenu.Trigger className="font-montsemibold flex items-center text-success text-2xl">
                        {menuitem.attributes.title} <CaretDownIcon className="w-8 h-8" aria-hidden />
                      </NavigationMenu.Trigger>
                      <NavigationMenu.Content className="mobile-sub-menu">
                        <div className="w-full">
                          <div className="flex flex-wrap">
                            {menudata.map((menuitems:any,index1:any)=>(
                              menuitems.attributes.parentMenu.data &&
                              <>
                                {menuitem.id == menuitems.attributes.parentMenu.data.id &&
                                  <div className="w-full mt-6" key={index1}>
                                  <Title title={menuitems.attributes.title} className="font-montregular text-xl text-successDark p-0 m-0 tracking-[0.3px]" level={5} isHTML></Title>
                                  <ul className="one m-0 list-none w-full">
                                    {menudata.map((menusubitems:any,index2:any)=>(
                                      menusubitems.attributes.parentMenu.data &&
                                        menusubitems.attributes.parentMenu.data.attributes.parentMenu.data &&
                                          menusubitems.attributes.parentMenu.data.id == menuitems.id &&
                                            <li className="pt-3" key={index2}>
                                              <Link href={menusubitems.attributes.link.url} className="text-sm sub-menu  transition-32s items-center flex flex-wrap leading-7 tracking-[0.48px] transition-32s hover:text-danger font-montsemibold">
                                                <div className="bg-muted3 rounded-md relative h-12 w-12">
                                                  {menusubitems.attributes.ico.data &&
                                                    <Image className="h-full w-full absolute p-1.5 object-contain object-center" fill={true} src={menusubitems.attributes.ico.data.attributes.url} alt={menusubitems.attributes.title}></Image>
                                                  }
                                                </div>
                                                <div className="pl-4 h-full w-[calc(100%-48px)] leading-[normal]">
                                                  {menusubitems.attributes.title &&
                                                    <span className="leading-[normal] text-sm font-montregular leading-7 tracking-[0.48px] text-successDark">{menusubitems.attributes.title}</span>
                                                  }
                                                  {menusubitems.attributes.description &&
                                                    <p className="leading-[normal] text-xs text-info transition-32s tracking-[0.38px]">
                                                      {menusubitems.attributes.description}
                                                    </p>
                                                  }
                                                </div>
                                              </Link>
                                            </li>
                                    ))}
                                  </ul>
                                </div>
                                }
                              </>
                            ))}
                          </div>
                        </div>
                      </NavigationMenu.Content>
                    </NavigationMenu.Item>
                }
              </>
            ))}
          </NavigationMenu.List>

          <NavigationMenu.List className="menu-list right-menu">
            {topcta.map((items:any,index:any) => (
              items.id == 1 ?
                (
                  <NavigationMenu.Item className="mt-5" key={index}>
                    <NavigationMenu.Link target={`${items.attributes.link.newWindow ? "_blank" : ""}`} className="font-montsemibold text-danger text-2xl" href={items.attributes.link.url}>
                      {items.attributes.link.text}
                    </NavigationMenu.Link>
                  </NavigationMenu.Item>
                )
              :
                items.id == 2 &&
                  (
                    <NavigationMenu.Item className="mt-5" key={index}>
                      <NavigationMenu.Link target={`${items.attributes.link.newWindow ? "_blank" : ""}`} className="font-montsemibold text-2xl text-danger tracking-[0.28px] text-danger py-4 px-6 rounded-md border-2 border-solid border-danger inline-block hover:text-muted2 hover:bg-danger transition-32s" href={items.attributes.link.url}>
                        {items.attributes.link.text}
                      </NavigationMenu.Link>
                    </NavigationMenu.Item>
                  )
            ))}
          </NavigationMenu.List>
        </NavigationMenu.Root>
      </div>
    </>
  );
}

export default Sidebar;