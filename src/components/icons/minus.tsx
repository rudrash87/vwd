export default function Minus() {
    return (
        <div className="mr-6 bg-muted4 rounded-md">
            <svg className="p-2" version="1.0" xmlns="http://www.w3.org/2000/svg"
                width="55px" height="55px" viewBox="0 0 512.000000 512.000000"
                preserveAspectRatio="xMidYMid meet">

                <g transform="translate(0.000000,512.000000) scale(0.100000,-0.100000)"
                    fill="#5435AB" stroke="none">
                    <path d="M240 2856 c-95 -20 -172 -83 -212 -174 -79 -178 33 -387 225 -421 89
                    -15 4528 -16 4613 0 103 19 184 82 226 177 79 178 -33 387 -225 421 -94 16
                    -4551 14 -4627 -3z"/>
                </g>
            </svg>
        </div>
    )
}