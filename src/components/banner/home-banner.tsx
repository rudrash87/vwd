"use client"
import CTA from "@/components/ui/cta ";
import { getMulticolorTitle, getMainTitle } from "../ui/helper";

export default function HomeBanner(Props: any) {
    const bannerdata = Props.bannerdata;
    const multititle = getMulticolorTitle(bannerdata.attributes.headerSection.title);
    const title = getMainTitle(bannerdata.attributes.headerSection.title);
    const btndata = [
        {
            id: bannerdata.attributes.headerSection.button1.id,
            bvariant: "isdark",
            label: bannerdata.attributes.headerSection.button1.text,
            url: bannerdata.attributes.headerSection.button1.url,
            className: "dark-btn",
            newwindow: bannerdata.attributes.headerSection.button1.newWindow,
        },
        {
            id: bannerdata.attributes.headerSection.button2.id,
            bvariant: "transparent",
            label: bannerdata.attributes.headerSection.button2.text,
            url: bannerdata.attributes.headerSection.button2.url,
            className: "trans-btn",
            newwindow: bannerdata.attributes.headerSection.button2.newWindow,
        }
    ];

    return (
        <CTA title={title} Multicolor={multititle} titlelevel={1} variant="full-w" btndata={btndata}></CTA>
    )
}