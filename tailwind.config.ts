import type { Config } from 'tailwindcss'
import plugin from 'tailwindcss';

const { fontFamily } = require('tailwindcss/defaultTheme');

const config: Config = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    './src/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {

      screens: {
        "sm": "576px",
        "md": "768px",
        "lg": "992px",
        "xl": "1200px",
        "2xl": "1400px",
        "3xxl": "1530px",
        "3xl": "1600px",
        "4xl": "1800px",
      },

      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
      fontFamily: {
        'montblack': ['var(--font-montblack)'],
        'montblackitalic': ['var(--font-montblackitalic)'],
        'montbold': ['var(--font-montbold)'],
        'montbolditalic': ['var(--font-montbolditalic)'],
        'montbook': ['var(--font-montbook)'],
        'montbookitalic': ['var(--font-montbookitalic)'],
        'montextralight': ['var(--font-montextralight)'],
        'montextralightitalic': ['var(--font-montextralightitalic)'],
        'monthairline': ['var(--font-monthairline)'],
        'monthairlineitalic': ['var(--font-monthairlineitalic)'],
        'montheavy': ['var(--font-montheavy)'],
        'montheavyitalic': ['var(--font-montheavyitalic)'],
        'montlight': ['var(--font-montlight)'],
        'montlightitalic': ['var(--font-montlightitalic)'],
        'montregular': ['var(--font-montregular)'],
        'montregularitalic': ['var(--font-montregularitalic)'],
        'montsemibold': ['var(--font-montsemibold)'],
        'montsemibolditalic': ['var(--font-montsemibolditalic)'],
        'montthin': ['var(--font-montthin)'],
        'montthinitalic': ['var(--font-montthinitalic)'],
      },

      get container() {
        return this._container;
      },

      set container(value) {
        this._container = value;
      },

      colors: {
        primary: "var(--primary)",
        secondary: "var(--secondary)",
        whiteDark: "var(--whiteDark)",
        success: "var(--success)",
        successDark: "var(--successDark)",
        info: "var(--info)",
        info2: "var(--info2)",
        info3: "var(--info3)",
        info4: "var(--info4)",
        info5: "var(--info5)",
        danger: "var(--danger)",
        danger2: "var(--danger2)",
        cyan: "var(--cyan)",
        indigo: "var(--indigo)",
        background: "var(--background)",
        backgroundDark: "var(--backgroundDark)",
        muted: "var(--muted)",
        muted2: "var(--muted2)",
        muted3: "var(--muted3)",
        muted4: "var(--muted4)",
        muted5: "var(--muted5)",
        muted6: "var(--muted6)",
        muted7: "var(--muted7)",
        black: "var(--black)",
      },

      animation: {
        scaleIn: 'scaleIn 200ms ease',
        scaleOut: 'scaleOut 200ms ease',
        fadeIn: 'fadeIn 200ms ease',
        fadeOut: 'fadeOut 200ms ease',
        enterFromLeft: 'enterFromLeft 250ms ease',
        enterFromRight: 'enterFromRight 250ms ease',
        exitToLeft: 'exitToLeft 250ms ease',
        exitToRight: 'exitToRight 250ms ease',
      },
    },
  },
  plugins: [

  ],
}
export default config
